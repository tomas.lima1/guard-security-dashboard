@extends('layouts.app')

@section('page-header')
    @include('layouts.page-header', [
        'pageHeadTitle' => 'Service Topology',
        'breadcrumbs' => [
            [
                'name' => 'Topology',
                'link' => '',
                'icon' => 'fas fa-code-branch'
            ]
        ]
    ])
@endsection
@section('content')
    <div class="row row-topology topology-card">
        <div class="card col-xl-8 col-topology px-0">
            <div class="card-header bg-transparent header-elements-inline">
                <h5 class="card-title">Topology</h5>
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                    </div>
                </div>
            </div>
            <div class="card-body bg-white topology-card">
                @if($error)
                    <div class="w-100 alert alert-warning " role="alert">
                        <h6 class="alert-heading">Data error!</h6>
                        <p>
                            The current data in the CB Manager is not enough. The Service Topology requires that
                            the Execution Environment, Network Link and Connection indexes have data.
                        </p>
                    </div>
                @else
                    <div id="chart" class="w-100 topology-card"></div>
                @endif
            </div>
        </div>
        <div class="col-xl-4 topology-info">
            <div class="card m-0">
                <div class="card-header bg-transparent header-elements-inline">
                    <h5 class="card-title">Service Details</h5>
                    <div class="header-elements">
                        <div class="list-icons">
                            <a class="list-icons-item" data-action="collapse"></a>
                        </div>
                    </div>
                </div>
                <div class="card-body mb-4 ">
                    <ul class="service-info list-group p-2">
                        <span class="alert alert-info">
                            Select a service to get more information.
                        </span>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    @include('service-topology.js.cytoscape')
    @include('service-topology.js.cytoscape-functions')
    <script type="text/javascript">
        const env = JSON.parse('{!! json_encode($environments) !!}');
        const con = JSON.parse('{!! json_encode($connections) !!}');
        const net = JSON.parse('{!! json_encode($networks) !!}');
    </script>
@endsection

@section('stylesheets')
    <style>
        .topology-card{
            width:  100%;
            height: 100%;
            margin: 0;
        }

        .bg-specific{
            background-color: #eeeded;
        }

        li.heading {
            background: #ededed;
        }
    </style>
@endsection