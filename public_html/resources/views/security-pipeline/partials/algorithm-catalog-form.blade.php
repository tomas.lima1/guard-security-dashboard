<fieldset class="card-body">
    <div class="row py-3">
        <div class="col-12">
            <div>
                <h6 class="font-weight-bold">Available Algorithms</h6>
            </div>
            <div class="table-responsive algorithm-list-form">
                <table class="table table-sm table-bordered tableFixHead" id="algoPipelineFormTable" style="border-top:none;">
                    <thead>
                    <tr style="height: 42px;">
                        <th class="text-center">#</th>
                        <th class="text-center">ID</th>
                        <th colspan="5" class="text-center">Description</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($algorithmWiseInsAndEnv as $algoKey=>$algo)
                        <tr id="algorithm_{!! $algo['algorithms']['id'] !!}">
                            <td style="text-align: -webkit-center !important; width: 100px;">
                                <div class="align-content-center">
                                    <input type="radio" name="algorithm_id" value="{!! $algo['algorithms']['id'] !!}"
                                           class="form-check-input-styled" id="{!! $algo['algorithms']['id'] !!}"

                                           @if(isset($pipeline))
                                           {!! (isset($pipeline['algorithm_catalog_id']) && $pipeline['algorithm_catalog_id'] === $algo['algorithms']['id']) ? 'checked': "" !!}
                                           @else
                                           {!! ($algoKey === 0) ? "" : "" !!}
                                           @endif
                                           data-fouc>
                                </div>
                            </td>
                            <td class="pl-3">{!! $algo['algorithms']['id'] !!}</td>
                            <td class="pl-3" colspan="5">
                                @if(isset($algo['algorithms']['description']))
                                    {!! $algo['algorithms']['description'] !!}
                                @else
                                    -
                                @endif
                            </td>
                        </tr>
                        @if(!empty($algo['instance']) && isset($algo['instance']))

                            <tr id="algorithm_env_{!! $algo['algorithms']['id'] !!}" class="algorithm_info env-table">
                                <td colspan="4" style="padding: 0 0 0 0;" class="env-table">
                                    <table class="table table-env table-sm borderless">
                                        @foreach($algo['instance'] as $insKey=>$instance)
                                            @php
                                                $agentInstanceParams = isset($algo['algorithms']['parameters']) ? $algo['algorithms']['parameters']: null;
                                                $algorithmsParams = htmlspecialchars(json_encode($agentInstanceParams), ENT_QUOTES, 'UTF-8');
                                                $checked = false;
                                                $algorithmInstanceId = $instance['id'];

                                                if(!empty($pipeline)){
                                                    if(!empty($pipeline['algorithm_configs']) && is_array($pipeline['algorithm_configs'])) {
                                                        $pipelineInstances = array_column($pipeline['algorithm_configs'], 'algorithm_instance_id');
                                                        if(in_array($algorithmInstanceId, $pipelineInstances)){
                                                            $checked = true;
                                                        }
                                                    }
                                                }
                                            @endphp

                                            @if(!empty($instance['duplicate']))
                                                <tr data-toggle="tooltip" data-html="true" title="Currently used by pipeline: <i>{!! $instance['pipelineName'] !!}</i>" {!!  ($instance['duplicate']) ? 'style="background-color:#DEDEDE;"' : ""; !!}>
                                            @else
                                                <tr>
                                            @endif
                                                    <td class="text-center align-baseline parsley-custom-container" id="{!! $instance['id'] !!}" style="width: 100px; text-align: -webkit-center !important;">
                                                        @if(!empty($instance['duplicate']) && !$checked)
                                                            <span><i class="fas fa-lock custom-lock"></i></span>
                                                        @else
                                                            <div class="align-content-center">
                                                                <input type="hidden" name="algorithm_instance_param" id="instance-{!! $algo['algorithms']['id'] !!}-{!! $instance['id'] !!}" value='{!! $algorithmsParams !!}'>
                                                                <input type="checkbox" value="{!! $instance['id'] !!}" {!! (isset($instance['duplicate']) && !$checked) ? 'disabled' : "" !!} class="form-check-input-styled"
                                                                       name="algorithm_environment_id[]"
                                                                       @if(isset($pipeline))
                                                                       {!! ($checked) ? 'checked' : "" !!}
                                                                       @else
                                                                       {!! ($algoKey === 0) ? "" : "" !!}
                                                                       @endif
                                                                       data-fouc>
                                                            </div>
                                                        @endif
                                                    </td>
                                                    <td>{!! $instance['id'] !!} </td>
                                                    <td>{!! isset($instance['endpoint']) ? $instance['endpoint'] : "-" !!}</td>
                                                    <td>{!! isset($instance['description']) ? $instance['description'] : "-" !!}</td>
                                                    <td>{!! isset($instance['type_description']) ? $instance['type_description']['name'] : "-" !!}</td>
                                                    <td>{!! isset($instance['partner']) ? $instance['partner'] : "-" !!}</td>
                                                </tr>
                                        @endforeach
                                    </table>
                                </td>
                            </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</fieldset>
<fieldset class="card-body">
    <div class="row pt-3">
        <div class="col-12">
            <h6 class="font-weight-bold">Parameters for selected algorithm</h6>
            <div class="text-center" id="algorithmParameter">{{--Algorithm paramertes populated dynamically--}}</div>
        </div>
    </div>
</fieldset>
