<style>
    .parsley-errors-list {
        list-style: none;
        color: red;
        padding-left: 0;
    }

    .form-control{
        display: block;
        width: 100%;
        height: calc(1.5385em + .875rem + 2px);
        padding: .4375rem .875rem;
        font-size: .8125rem;
        font-weight: 400;
        line-height: 1.5385;
        color: #333333;
        background-color: #fff;
        background-clip: padding-box;
        border: 1px solid #ddd !important;
        border-radius: .1875rem;
        box-shadow: 0 0 0 0 transparent;
        transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
    }

    .form-control:focus{
        border-top-color: #007065 !important;
        border-top: 1px;
    }

    .btn-lower{
        text-transform: none;
    }

    .w-message{
        width: 310px;
    }

</style>