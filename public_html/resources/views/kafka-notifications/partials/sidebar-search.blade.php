<div class="card">
    <div class="card-header bg-transparent header-elements-inline">
        <span class="text-uppercase font-size-sm font-weight-semibold">Search</span>
        <div class="header-elements">
            <div class="list-icons">
                <a class="list-icons-item" data-action="collapse"></a>
            </div>
        </div>
    </div>

    <div class="card-body">
        <form action="#">
            <div class="form-group-feedback form-group-feedback-right">
                <input type="search" id="searchBar" class="form-control" placeholder="Search">
                <div class="form-control-feedback">
                    <i class="icon-search4 font-size-base text-muted"></i>
                </div>
            </div>
        </form>
    </div>
</div>