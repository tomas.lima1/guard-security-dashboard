<div class="card">
    <div class="card-header bg-transparent header-elements-inline">
        <span class="text-uppercase font-size-sm font-weight-semibold">Filter by Source</span>
        <div class="header-elements">
            <div class="list-icons">
                <a class="list-icons-item" data-action="collapse"></a>
            </div>
        </div>
    </div>

    <div class="card-body px-lg-0 px-xl-2">
        <form action="#">
            <div class="form-group sources-filter">

            </div>
        </form>
    </div>
</div>