<div class="card">
    <div class="card-header bg-transparent header-elements-inline py-2">
        <h5 class="card-title">Notifications</h5>
        <div class="header-elements">
            <div class="list-icons">
                <a class="list-icons-item" data-action="collapse"></a>
            </div>
        </div>
    </div>
    <div class="card-body">
        <table class="table table-sm table-bordered" id="notifications_table" style="border-top: 1px solid lightgray">
            <thead>
                <tr>
                    <th scope="col">Source</th>
                    <th scope="col">Severity</th>
                    <th scope="col">Description</th>
                    <th scope="col">Data</th>
                    <th scope="col">Timestamp</th>
                    <th scope="col">Date/Time</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>