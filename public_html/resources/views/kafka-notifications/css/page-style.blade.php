<style>
    .btn-primary:hover{
        background-color: #12439B;
    }

    .dataTables_paginate .paginate_button.current, .dataTables_paginate .paginate_button.current:focus, .dataTables_paginate .paginate_button.current:hover{
        color: #fff;
        background-color: #12439B;
    }

    th, td { padding: 8px 16px; }
    th     { background:#eee; }


</style>