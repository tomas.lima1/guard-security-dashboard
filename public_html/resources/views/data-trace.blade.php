@extends('layouts.app')

@section("content")
  <div id="data-tracking"></div>
@endsection

@section('scripts')
  <script src="{{ mix('js/app.js') }}"></script>
@endsection
