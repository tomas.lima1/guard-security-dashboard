import { useMemo } from 'react';
import { Button, Modal } from 'react-bootstrap';
import CardBasic from './components/CardBasic';
import Header from './components/Header';
import Table from './components/table';
import useDepartments from './hooks/useDepartments';

export const Departments = () => {
  const {
    data,
    columns,
    openDetails,
    setOpenDetails,
    menuActions,
    selectedDepartment,
    history,
    ...queryProps
  } = useDepartments();
  const departments = useMemo(() => data?.data, [data]);

  return (
    <>
      <Header
        sectionTitle="User Data Traceability"
        headerSubtitle="Departments endpoints"
        currentRoute="Departments"
      />
      <CardBasic title="Departments" actions={menuActions}>
        <Table
          data={departments || []}
          columns={columns}
          pageSizeOptions={[5, 10, 20, 40]}
          {...queryProps}
        />
      </CardBasic>
      {openDetails && selectedDepartment && (
        <Modal size="lg" show={openDetails} onHide={() => setOpenDetails(false)} centered>
          <Modal.Header className="bg-primary" closeButton>
            <Modal.Title>
              Department: <strong>{selectedDepartment.name}</strong>
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <pre>{JSON.stringify(selectedDepartment, null, '\t')}</pre>
          </Modal.Body>
          <Modal.Footer>
            <Button
              title="Open full details"
              onClick={() => history.push(`/department/${selectedDepartment.id}`)}
              variant="outline-secondary"
            >
              <i className="icon-enlarge7"></i>
            </Button>
          </Modal.Footer>
        </Modal>
      )}
    </>
  );
};

export default Departments;
