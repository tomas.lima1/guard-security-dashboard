import { API_URL } from '../config/data-tracking.config';
import axios from 'axios';
import { Request } from '../models/request';

const cancelRequest = async (requestId: string) => {
  const url = `${API_URL}/api/request/${requestId}/cancel`;
  const res = await axios.patch<{ data: Request }>(url);

  return res.data;
};

export default cancelRequest;
