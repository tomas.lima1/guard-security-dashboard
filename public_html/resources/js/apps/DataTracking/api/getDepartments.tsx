import { API_URL } from '../config/data-tracking.config';
import axios from 'axios';
import { Department } from '../models/department';

const getDepartments = async (skip?: number, limit?: number, sort = '', search = '') => {
  const url = `${API_URL}/api/department`;
  const res = await axios.get<{ count: number; data: Department[] }>(url, {
    params: {
      skip,
      limit,
      search,
      sort: !sort ? undefined : sort.includes('-') ? [`${sort.substr(1)},asc`] : [`${sort},desc`],
    },
  });

  return res.data;
};

export default getDepartments;
