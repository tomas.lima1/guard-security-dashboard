import { API_URL } from '../config/data-tracking.config';
import axios from 'axios';
import qs from 'qs';
import { Request } from '../models/request';

const getRequests = async (skip: number, limit: number, sort = '', search = '') => {
  const url = `${API_URL}/api/request`;
  // TODO: Make sorting functionality for nested data
  const res = await axios.get<{ count: number; data: Request[] }>(url, {
    params: {
      skip,
      limit,
      search,
      sort: !sort ? undefined : sort.includes('-') ? [`${sort.substr(1)},asc`] : [`${sort},desc`],
    },
    paramsSerializer: (params) => {
      return qs.stringify(params);
    },
  });

  return res.data;
};

export default getRequests;
