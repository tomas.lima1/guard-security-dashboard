import { API_URL } from '../config/data-tracking.config';
import axios from 'axios';
import qs from 'qs';
import { PolicyRule } from '../models/policy-rule';

const getPolicyRules = async (
  policyId: string,
  skip?: number,
  limit?: number,
  sort = '',
  search = '',
) => {
  const url = `${API_URL}/api/policy/${policyId}/rule`;
  const res = await axios.get<{ count: number; data: PolicyRule[] }>(url, {
    params: {
      skip,
      limit,
      search,
      sort: !sort ? undefined : sort.includes('-') ? [`${sort.substr(1)},asc`] : [`${sort},desc`],
    },
    paramsSerializer: (params) => {
      return qs.stringify(params);
    },
  });

  return res.data;
};

export default getPolicyRules;
