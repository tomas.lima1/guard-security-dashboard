import { API_URL } from '../config/data-tracking.config';
import axios from 'axios';
import { Department } from '../models/department';

const updateDepartment = async (department: Department) => {
  const url = `${API_URL}/api/department`;
  const res = await axios.put<{ data: Department }>(url, department);

  return res.data;
};

export default updateDepartment;
