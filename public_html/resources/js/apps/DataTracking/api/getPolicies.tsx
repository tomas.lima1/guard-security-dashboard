import { API_URL } from '../config/data-tracking.config';
import axios from 'axios';
import qs from 'qs';
import { Policy } from '../models/policy';

const getPolicies = async (skip: number, limit: number, sort = '', search = '') => {
  const url = `${API_URL}/api/policy`;
  const res = await axios.get<{ count: number; data: Policy[] }>(url, {
    params: {
      skip,
      limit,
      search,
      sort: !sort ? undefined : sort.includes('-') ? [`${sort.substr(1)},asc`] : [`${sort},desc`],
    },
    paramsSerializer: (params) => {
      return qs.stringify(params);
    },
  });

  return res.data;
};

export default getPolicies;
