import { API_URL } from '../config/data-tracking.config';
import axios from 'axios';
import { Policy } from '../models/policy';

const deletePolicy = async (policyId: string) => {
  const url = `${API_URL}/api/policy/${policyId}`;
  const res = await axios.delete<{ data: Policy }>(url);

  return res.data;
};

export default deletePolicy;
