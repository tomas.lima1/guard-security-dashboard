import { API_URL } from '../config/data-tracking.config';
import axios from 'axios';
import { Policy } from '../models/policy';

const getPolicy = async (policyId: string) => {
  const url = `${API_URL}/api/policy/${policyId}`;
  const res = await axios.get<{ data: Policy }>(url);

  return res.data;
};

export default getPolicy;
