import { API_URL } from '../config/data-tracking.config';
import axios from 'axios';
import { Policy } from '../models/policy';

const updatePolicy = async (policy: Policy) => {
  const url = `${API_URL}/api/policy`;
  const res = await axios.put<{ data: Policy }>(url, policy);

  return res.data;
};

export default updatePolicy;
