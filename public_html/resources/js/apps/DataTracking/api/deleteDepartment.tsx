import { API_URL } from '../config/data-tracking.config';
import axios from 'axios';
import { Department } from '../models/department';

const deleteDepartment = async (departmentId: string) => {
  const url = `${API_URL}/api/department/${departmentId}`;
  const res = await axios.delete<{ data: Department }>(url);

  return res.data;
};

export default deleteDepartment;
