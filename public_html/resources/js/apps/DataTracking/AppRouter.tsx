import { BrowserRouter as Router, Redirect, Route, Switch } from 'react-router-dom';
import {
  ExtendedStringifyOptions,
  QueryParamProvider,
  transformSearchStringJsonSafe,
} from 'use-query-params';
import Department from './Department';
import Departments from './Departments';
import Policies from './Policies';
import Policy from './Policy';
import PolicyRules from './PolicyRules';
import Requests from './Requests';

const queryStringifyOptions: ExtendedStringifyOptions = {
  transformSearchString: transformSearchStringJsonSafe,
};

export const AppRouter = () => {
  return (
    <Router basename="/data-trace" data-testid="router">
      <QueryParamProvider ReactRouterRoute={Route} stringifyOptions={queryStringifyOptions}>
        <Switch>
          <Route exact path="/requests" component={Requests} />
          <Route exact path="/policies" component={Policies} />
          <Route exact path="/policy/:id" component={Policy} />
          <Route exact path="/policy/:id/rules" component={PolicyRules} />
          <Route exact path="/departments" component={Departments} />
          <Route exact path="/department/:id" component={Department} />
          <Redirect exact to="/requests" />
        </Switch>
      </QueryParamProvider>
    </Router>
  );
};
