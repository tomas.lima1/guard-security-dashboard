import { useMemo } from 'react';
import { Button, Modal } from 'react-bootstrap';
import CardBasic from './components/CardBasic';
import Header from './components/Header';
import Table from './components/table';
import usePolicies from './hooks/usePolicies';
import PolicyRulesPreview from './PolicyRulesPreview';

const Policies = () => {
  const {
    data,
    columns,
    selectedPolicy,
    menuActions,
    openRule,
    openDetails,
    setOpenDetails,
    history,
    handleOnCloseRulesModal,
    ...queryProps
  } = usePolicies();
  const policies = useMemo(() => data?.data, [data]);

  return (
    <>
      <Header
        sectionTitle="User Data Traceability"
        headerSubtitle="Example subtitle"
        currentRoute="Policies"
      />
      <CardBasic title="Policies" actions={menuActions}>
        <Table
          data={policies || []}
          columns={columns}
          pageSizeOptions={[5, 10, 20, 40]}
          {...queryProps}
        />
      </CardBasic>
      {openRule && selectedPolicy && (
        <PolicyRulesPreview
          selectedPolicy={selectedPolicy}
          show={openRule}
          onHide={handleOnCloseRulesModal}
        />
      )}
      {openDetails && selectedPolicy && (
        <Modal size="lg" show={openDetails} onHide={() => setOpenDetails(false)} centered>
          <Modal.Header className="bg-primary" closeButton>
            <Modal.Title>
              Policy: <strong>{selectedPolicy!.name}</strong>
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <pre>{JSON.stringify(selectedPolicy, null, '\t')}</pre>
          </Modal.Body>
          <Modal.Footer>
            <Button
              title="Open full details"
              onClick={() => history.push(`/policy/${selectedPolicy.id}`)}
              variant="outline-secondary"
            >
              <i className="icon-enlarge7"></i>
            </Button>
          </Modal.Footer>
        </Modal>
      )}
    </>
  );
};

export default Policies;
