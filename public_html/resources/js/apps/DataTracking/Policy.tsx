import { Button, Col, Container, Form, Row } from 'react-bootstrap';
import Header from './components/Header';
import CardBasic from './components/CardBasic';
import usePolicy from './hooks/usePolicy';
import FormControlFeedback from './components/FormControlFeedback';
import InputCustomForId from './components/form/InputCustomForId';
import { FormProvider } from 'react-hook-form';
import InputEnableSwitch from './components/form/InputEnableSwitch';
import getDepartments from './api/getDepartments';
import { useQuery } from 'react-query';

const Policy = () => {
  const methods = usePolicy();
  const {
    isEdit,
    id,
    handleSubmit,
    onSubmit,
    register,
    history,
    createMutation,
    updateMutation,
    state: { errors },
  } = methods;

  const { data: departments } = useQuery(['departments'], () => getDepartments(0, 10, '', ''), {
    keepPreviousData: true,
  });

  return (
    <>
      <Header
        sectionTitle="User Data Traceability"
        headerSubtitle="Policies control"
        currentRoute={!isEdit ? 'Create policy' : 'Edit policy'}
      />
      <CardBasic
        title={`${isEdit ? `Policy ID: <strong>${id}</strong>` : 'Add a new policy endpoint'}`}
      >
        <Container>
          <Row className="justify-content-center">
            <Col lg="8">
              <FormProvider {...methods}>
                <Form autoComplete="off" onSubmit={handleSubmit(onSubmit)}>
                  <Form.Group as={Row} controlId="id">
                    <Form.Label column>Policy Id</Form.Label>
                    <Col sm="9">
                      <InputCustomForId disabled={isEdit} name="id" />
                    </Col>
                  </Form.Group>
                  <Form.Group as={Row} controlId="name">
                    <Form.Label column>Name</Form.Label>
                    <Col sm="9">
                      <Form.Control
                        type="text"
                        placeholder="(*) A name for your policy"
                        {...register('name')}
                        isInvalid={!!errors.name}
                      />
                      <FormControlFeedback errors={errors} name="name" />
                    </Col>
                  </Form.Group>

                  <Form.Group as={Row} controlId="path">
                    <Form.Label column>Path</Form.Label>
                    <Col sm="9">
                      <Form.Control
                        type="text"
                        placeholder="(*) path related to the policy"
                        {...register('path')}
                        isInvalid={!!errors.path}
                      />
                      <FormControlFeedback errors={errors} name="path" />
                    </Col>
                  </Form.Group>

                  {departments && (
                    <Form.Group as={Row} controlId="departmentId">
                      <Form.Label column>Department</Form.Label>
                      <Col sm="9">
                        <Form.Control
                          as="select"
                          {...register('departmentId')}
                          isInvalid={!!errors.departmentId}
                          defaultValue=""
                        >
                          <option value="" disabled hidden>
                            Select a department
                          </option>

                          {departments.data.map((department: any) => (
                            <option key={department.id} value={department.id}>
                              {department.name}
                            </option>
                          ))}
                        </Form.Control>

                        <FormControlFeedback errors={errors} name="departmentId" />
                      </Col>
                    </Form.Group>
                  )}

                  <Form.Group as={Row} controlId="description">
                    <Form.Label column>Description</Form.Label>
                    <Col sm="9">
                      <Form.Control
                        as="textarea"
                        aria-describedby="description"
                        rows={2}
                        placeholder="Describe the purpose of this policy"
                        {...register('description')}
                      />
                    </Col>
                  </Form.Group>
                  <Form.Group as={Row} controlId="priority">
                    <Form.Label column>Priority</Form.Label>
                    <Col sm="9">
                      <Form.Control
                        as="input"
                        type="number"
                        aria-describedby="priority"
                        {...register('priority')}
                        isInvalid={!!errors.priority}
                      />
                      <FormControlFeedback errors={errors} name="priority" />
                    </Col>
                  </Form.Group>
                  <Col sm={{ span: 9, offset: 3 }}>
                    <Form.Group as={Row} controlId="enabled">
                      <InputEnableSwitch name="enabled" label="Policy status" />
                    </Form.Group>
                  </Col>
                  <Col sm={{ span: 9, offset: 3 }}>
                    <Form.Group as={Row} controlId="enable">
                      <Col>
                        <Button
                          title="Go back to the preview page visited"
                          block
                          variant="outline-secondary"
                          type="button"
                          onClick={() => history.goBack()}
                        >
                          Back
                        </Button>
                      </Col>
                      <Col>
                        <Button
                          title={!isEdit ? 'Create a new policy' : 'Save changes'}
                          block
                          variant={!isEdit ? 'success' : 'info'}
                          type="submit"
                        >
                          {createMutation.isLoading || updateMutation.isLoading
                            ? 'loading...'
                            : !isEdit
                            ? 'Create'
                            : 'Edit'}
                        </Button>
                      </Col>
                    </Form.Group>
                  </Col>
                </Form>
              </FormProvider>
            </Col>
          </Row>
        </Container>
      </CardBasic>
    </>
  );
};

export default Policy;
