import ReactDOM from 'react-dom';
import { QueryClient, QueryClientProvider } from 'react-query';
import { ReactQueryDevtools } from 'react-query/devtools';
import { ToastProvider } from 'react-toast-notifications';
import { AppRouter } from './AppRouter';
import { GlobalStyles } from './components/styled';
import { API_URL } from './config/data-tracking.config';
import { DepartmentProvider } from './contexts/DepartmentContext';
import { PolicyProvider } from './contexts/PolicyContext';
import { RequestProvider } from './contexts/RequestContext';
import ErrorBoundary from './ErrorBoundary';

const queryClient = new QueryClient();

export default function DataTracking() {
  return (
    <ErrorBoundary>
      <QueryClientProvider client={queryClient}>
        <DepartmentProvider>
          <PolicyProvider>
            <RequestProvider>
              <ToastProvider>
                <GlobalStyles>
                  <AppRouter />
                </GlobalStyles>
                <ReactQueryDevtools initialIsOpen={false} />
              </ToastProvider>
            </RequestProvider>
          </PolicyProvider>
        </DepartmentProvider>
      </QueryClientProvider>
    </ErrorBoundary>
  );
}

if (document.getElementById('data-tracking')) {
  ReactDOM.render(<DataTracking />, document.getElementById('data-tracking'));
}
