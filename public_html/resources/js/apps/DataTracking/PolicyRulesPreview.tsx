import { format } from 'date-fns';
import { useMemo } from 'react';
import { Button, Col, Container, Modal, Row } from 'react-bootstrap';
import { useQuery } from 'react-query';
import { useHistory } from 'react-router';
import styled from 'styled-components';
import getPolicyRules from './api/getPolicyRules';
import useToastAlerts from './hooks/useToastAlerts';
import { Policy } from './models/policy';
import { PolicyRule } from './models/policy-rule';

type Props = {
  selectedPolicy: Policy;
  show: boolean;
  onHide: any;
};
const RuleHeaderWrapper = styled.div`
  display: flex;
  justify-content: space-around;

  @media screen and (max-width: 720px) {
    display: block;
    text-align: left;
  }
`;
const RuleContainer = styled(Row)`
  display: flex;
  align-items: baseline;
`;

const PolicyRulesPreview = ({ selectedPolicy, show, onHide }: Props) => {
  const history = useHistory();
  const { addToast } = useToastAlerts();
  const { isLoading, data } = useQuery(
    ['policyRules', selectedPolicy.id],
    () => getPolicyRules(selectedPolicy.id),
    {
      keepPreviousData: true,
      onError: ({ error }) =>
        addToast(`An erorr has occurred, please try again in few seconds!`, {
          appearance: 'error',
          autoDismiss: true,
        }),
    },
  );
  const policyRules = useMemo(() => data?.data, [data]);

  return (
    <Modal size="lg" show={show} onHide={onHide} centered>
      <Modal.Header className="bg-guard" closeButton>
        <Modal.Title>
          Policy's rules: <strong className="ml-4">{selectedPolicy.name}</strong>
          <Button
            variant="link"
            className="text-light"
            onClick={() => history.push(`/policy/${selectedPolicy.id}`)}
          >
            <i className="icon-pencil7 ml-3"></i>
          </Button>
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Container>
          <Row className="justify-content-center">
            <Col lg="10">
              <fieldset>
                <legend>Description:</legend>
                {selectedPolicy.description}
              </fieldset>
            </Col>
          </Row>
          <Row className="justify-content-center mt-4">
            <Col
              lg="10"
              className="card-group-control card-group-control-right"
              id={`rules-accordion`}
            >
              {isLoading && <Col className="text-center">...loading rules</Col>}
              <fieldset>
                <legend>Rules:</legend>

                {!isLoading &&
                  policyRules &&
                  policyRules.map((item: PolicyRule, index: number) => (
                    <RuleContainer key={item.id}>
                      <Col>
                        <div className="card">
                          <div className="card-header bg-secondary">
                            <h6 className="card-title">
                              <a
                                data-toggle="collapse"
                                className="collapsed text-white w-100"
                                href={`#accordion-${index}`}
                              >
                                <RuleHeaderWrapper>
                                  <div>{item.name}</div>
                                  <div>{format(new Date(item.createdAt), 'Pp')}</div>
                                  <div>
                                    Priority <b>{item.priority}</b>{' '}
                                  </div>
                                </RuleHeaderWrapper>
                              </a>
                            </h6>
                          </div>

                          <div
                            id={`accordion-${index}`}
                            className="collapse"
                            data-parent="#rules-accordion"
                          >
                            <div className="card-body">
                              <pre>{JSON.stringify(item, null, '\t')}</pre>
                            </div>
                          </div>
                        </div>
                      </Col>
                    </RuleContainer>
                  ))}
              </fieldset>
            </Col>
          </Row>
        </Container>
      </Modal.Body>
      <Modal.Footer>
        <Button
          className="d-block"
          title="Open full details"
          onClick={() =>
            history.push({
              pathname: `/policy/${selectedPolicy?.id}/rules`,
              state: {
                selectedPolicy,
              },
            })
          }
          variant="outline-secondary"
        >
          <i className="icon-enlarge7"></i>
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default PolicyRulesPreview;
