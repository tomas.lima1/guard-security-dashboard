export interface Department {
  id: string;
  createdAt: Date;
  updatedAt: Date;
  metadata: Record<string, any>;
  name: string;
  description: string | null;
  endpoint: string;
  enabled: boolean;
}
