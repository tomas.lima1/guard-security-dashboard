import { IPolicyRule } from './rule';

export interface IPolicy {
  id: string;
  name: string;
  description: string;
  priority: number;
  rules: IPolicyRule[];
  createdAt?: Date;
  updatedAt?: Date;
  createdBy: {
    id: string;
    email: string;
  };
  auto?: boolean;
}
