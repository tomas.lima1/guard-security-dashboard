export type PolicyRule = {
  id: string;
  createdAt: Date;
  updatedAt: Date;
  metadata: Record<string, null> | null;
  name: string;
  description: string | null;
  regoPayload: string | null;
  priority: number;
  policyId: string;
  enabled: boolean;
};
