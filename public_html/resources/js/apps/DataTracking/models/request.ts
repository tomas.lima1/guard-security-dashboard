export type Request = {
  id: string;
  createdAt: Date;
  updatedAt: Date;
  metadata: Record<string, null>;
  status: RequestStatus;
  policyRuleId: string;
  requestorId: string;
  addressedToId: string;
};

export let RequestStatus: {
  CREATED: 'CREATED';
  PENDING: 'PENDING';
  APPROVED: 'APPROVED';
  DENIED: 'DENIED';
  CANCELED: 'CANCELED';
};

export type RequestStatus = typeof RequestStatus[keyof typeof RequestStatus];
