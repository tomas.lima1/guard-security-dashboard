export interface IPolicyRule {
  id: string;
  name: string;
  description: string;
  priority: number;
  openPolicyAgent: string;
  createdAt: Date;
  updatedAt: Date;
}
