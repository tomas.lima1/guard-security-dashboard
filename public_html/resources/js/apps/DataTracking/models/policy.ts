import { PolicyRule } from './policy-rule';

export type Policy = {
  id: string;
  createdAt: Date;
  updatedAt: Date;
  metadata: Record<string, any> | null;
  name: string;
  path: string;
  departmentId: string;
  description: string | null;
  priority: number;
  policyRules: PolicyRule[];
  enabled: boolean;
};
