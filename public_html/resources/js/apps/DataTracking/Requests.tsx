import { useMemo } from 'react';
import { Modal } from 'react-bootstrap';
import CardBasic from './components/CardBasic';
import Header from './components/Header';
import Table from './components/table';
import useRequests from './hooks/useRequests';

const Requests = () => {
  const { data, error, columns, selectedRequest, openDetails, setOpenDetails, ...queryProps } =
    useRequests();
  const requests = useMemo(() => data?.data, [data]);

  return (
    <>
      <Header
        sectionTitle="User Data Traceability"
        headerSubtitle="Example subtitle"
        currentRoute="Patient requests"
      />
      <CardBasic title="Requests">
        <Table
          data={requests || []}
          columns={columns}
          pageSizeOptions={[5, 10, 20, 40]}
          {...queryProps}
        />
      </CardBasic>
      {selectedRequest && (
        <Modal size="lg" show={openDetails} onHide={() => setOpenDetails(false)} centered>
          <Modal.Header className="bg-primary" closeButton>
            <Modal.Title>
              Request: <strong>{selectedRequest.id}</strong>
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <pre>{JSON.stringify(selectedRequest, null, '\t')}</pre>
          </Modal.Body>
        </Modal>
      )}
    </>
  );
};

export default Requests;
