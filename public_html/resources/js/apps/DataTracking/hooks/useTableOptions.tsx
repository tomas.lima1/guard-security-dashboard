import { useCallback, useState } from 'react';
import sortToString from '../helpers/sortToString';

export type HandleFetchProps = {
  sortBy: Array<any>;
  pageSize: number;
  pageIndex: number;
  globalFilter: string;
};
const useTableOptions = () => {
  const [pageIndex, setPageIndex] = useState(1);
  const [pageSize, setPageSize] = useState(10);
  const [sort, setSort] = useState('');
  const [globalFilter, setGlobalFilter] = useState('');
  const [pageCount, setPageCount] = useState(0);

  const handleOnFetch = useCallback(
    ({ sortBy, pageSize, pageIndex, globalFilter }: HandleFetchProps) => {
      setSort(sortToString(sortBy));
      setPageSize(pageSize);
      setPageIndex(pageIndex);
      setGlobalFilter(globalFilter);
    },
    [],
  );

  return {
    pageIndex,
    pageSize,
    sort,
    globalFilter,
    handleOnFetch,
    pageCount,
    setPageCount,
  };
};

export default useTableOptions;
