import { yupResolver } from '@hookform/resolvers/yup';
import { useEffect, useMemo } from 'react';
import { useForm, useFormState } from 'react-hook-form';
import { useMutation, useQuery, useQueryClient } from 'react-query';
import { useHistory, useParams } from 'react-router-dom';
import * as yup from 'yup';
import createDepartment from '../api/createDepartment';
import getDepartment from '../api/getDepartment';
import updateDepartment from '../api/updateDepartment';
import { URL_REGEX } from '../config/data-tracking.config';
import { Department } from '../models/department';
import useToastAlerts from './useToastAlerts';

const DepartmentSchema = yup.object().shape({
  id: yup.string().when('auto', { is: false, then: yup.string().required() }),
  auto: yup.boolean(),
  name: yup.string().required(),
  endpoint: yup.string().matches(URL_REGEX, 'endpoint must be a valid URL').required(),
});

interface FormType extends Department {
  auto: boolean;
}

const useDepartment = () => {
  const { addToast } = useToastAlerts();
  const history = useHistory();
  const { id } = useParams<{ id: string }>();
  const isEdit = id !== '0';
  const queryClient = useQueryClient();
  const query = useQuery([`department-${id}`, id], () => getDepartment(id), {
    enabled: isEdit,
    keepPreviousData: true,
  });
  const { data } = query;
  const department = useMemo(() => data?.data, [data]);

  const formMethods = useForm<FormType>({
    defaultValues: {
      auto: true,
      name: '',
      description: '',
      endpoint: '',
      enabled: false,
    },
    resolver: yupResolver(DepartmentSchema),
  });

  const { control, setValue } = formMethods;
  const state = useFormState({ control });

  useEffect(() => {
    if (department) {
      setValue('id', department.id);
      setValue('name', department.name);
      setValue('description', department.description);
      setValue('endpoint', department.endpoint);
      setValue('enabled', department.enabled);
    }
  }, [department]);

  const createMutation = useMutation(createDepartment, {
    onMutate: async (newDepartment) => {
      // Cancel any outgoing refetches (so they don't overwrite our optimistic update)
      await queryClient.cancelQueries('departments');

      // Snapshot the previous value
      const previousDepartments =
        queryClient.getQueryData<{ count: number; data: Department[] }>('departments');

      // Optimistically update to the new value
      if (previousDepartments) {
        queryClient.setQueryData<{ count: number; data: Department[] }>('departments', {
          ...previousDepartments,
          data: [...previousDepartments.data, newDepartment],
        });
      }
      return { previousDepartments };
    },
    onSuccess: ({ data }) => {
      addToast('Department created correctly', { appearance: 'success', autoDismiss: true });
      history.replace(`/department/${data.id}`);
    },
    onError: (error: any, variable, context: any) => {
      addToast(error?.message as object, { appearance: 'error', autoDismiss: true });
      queryClient.setQueryData('departments', context.previousTodos);
    },
    onSettled: () => {
      queryClient.invalidateQueries('departments');
    },
  });
  const updateMutation = useMutation(updateDepartment, {
    onMutate: async (newDepartment) => {
      // Cancel any outgoing refetches (so they don't overwrite our optimistic update)
      await queryClient.cancelQueries('departments');

      // Snapshot the previous value
      const previousDepartments =
        queryClient.getQueryData<{ count: number; data: Department[] }>('departments');

      // Optimistically update to the new value
      if (previousDepartments) {
        queryClient.setQueryData<{ count: number; data: Department[] }>('departments', {
          ...previousDepartments,
          data: [...previousDepartments.data, newDepartment],
        });
      }
      return { previousDepartments };
    },
    onSuccess: () => {
      addToast('Department edited correctly', { appearance: 'success', autoDismiss: true });
    },
    onError: (error: any) => {
      addToast(error?.message as object, { appearance: 'error', autoDismiss: true });
    },
    onSettled: () => {
      queryClient.invalidateQueries('departments');
    },
  });

  const onSubmit = (data: FormType) => {
    const departmentId = id;
    if (isEdit) {
      const { id, auto, ...form } = data;
      updateMutation.mutate({ id: departmentId, ...form });
    } else {
      const { auto, ...form } = data;
      createMutation.mutate(form);
    }
  };

  return {
    isEdit,
    id,
    onSubmit,
    history,
    ...formMethods,
    state,
    query,
    createMutation,
    updateMutation,
  };
};

export default useDepartment;
