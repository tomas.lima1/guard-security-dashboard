import { yupResolver } from '@hookform/resolvers/yup';
import { useFieldArray, useForm, useFormState } from 'react-hook-form';
import { useQuery } from 'react-query';
import { useHistory } from 'react-router-dom';
import * as yup from 'yup';
import getPolicyRules from '../api/getPolicyRules';
import { Policy } from '../models/policy';
import { IPolicyRule } from '../models/rule';
import useToastAlerts from './useToastAlerts';

type Props = {
  selectedPolicy: Policy;
};

interface RuleForm extends IPolicyRule {
  auto: boolean;
}

type Form = { rules: IPolicyRule[] };

const RuleSchema = yup.object().shape({
  id: yup.string().when('auto', { is: false, then: yup.string().required() }),
  auto: yup.boolean(),
  name: yup.string().required(),
  description: yup.string(),
  priority: yup.number().positive().integer().required(),
  openPolicyAgent: yup.string().required(),
});

const usePolicyRules = ({ selectedPolicy }: Props) => {
  const { addToast } = useToastAlerts();
  const history = useHistory();
  const form = useForm<Form>({
    resolver: yupResolver(RuleSchema),
    defaultValues: {
      rules: [],
    },
  });
  const query = useQuery(
    ['policyRules', selectedPolicy.id],
    () => getPolicyRules(selectedPolicy.id),
    {
      keepPreviousData: true,
      onError: ({ error }) =>
        addToast(`An erorr has occurred, please try again in few seconds!`, {
          appearance: 'error',
          autoDismiss: true,
        }),
    },
  );

  const { control } = form;
  const state = useFormState({ control });
  const { fields } = useFieldArray({
    control,
    name: 'rules',
  });

  const onSubmit = (data: RuleForm) => {
    console.log(data);
  };

  return {
    onSubmit,
    history,
    ...form,
    state,
    ...query,
  };
};

export default usePolicyRules;
