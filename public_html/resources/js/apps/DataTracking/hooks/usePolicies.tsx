import { format } from 'date-fns';
import { useContext, useEffect, useMemo, useState } from 'react';
import { Badge, Button } from 'react-bootstrap';
import { useMutation, useQuery, useQueryClient } from 'react-query';
import { useHistory } from 'react-router';
import styled from 'styled-components';
import deletePolicy from '../api/deletePolicy';
import getPolicies from '../api/getPolicies';
import ActionButtons from '../components/ActionButtons';
import { MenuActions } from '../components/CardBasic';
import { PolicyContext } from '../contexts/PolicyContext';
import { Policy } from '../models/policy';
import useTableOptions from './useTableOptions';
import useToastAlerts from './useToastAlerts';

const ButtonStyled = styled(Button)`
  display: inline-flex;
  align-items: center;
  font-size: 0.8em;
  padding: 2px 5px;
  outline: none;
  border: none;
  border-radius: 0.5em;
`;

const BadgeStyled = styled(Badge)`
  display: inline-flex;
  align-items: center;
  justify-content: center;
`;
const usePolicies = () => {
  const { addToast } = useToastAlerts();
  const history = useHistory();
  const [openRule, setOpenRule] = useState(false);
  const [openDetails, setOpenDetails] = useState(false);
  const { policies, selectedPolicy, setPolicies, setSelectedPolicy } = useContext(PolicyContext);

  const { pageIndex, pageSize, sort, globalFilter, handleOnFetch, setPageCount, pageCount } =
    useTableOptions();

  const queryClient = useQueryClient();
  const query = useQuery(
    ['policies', pageIndex, pageSize, sort, globalFilter],
    () => getPolicies(pageIndex, pageSize, sort, globalFilter),
    {
      keepPreviousData: true,
      onError: ({ error }) =>
        addToast(`An erorr has occurred, please try again in few seconds!`, {
          appearance: 'error',
          autoDismiss: true,
        }),
    },
  );
  const deleteMutation = useMutation(deletePolicy, {
    onMutate: async (policyId) => {
      // Cancel any outgoing refetches (so they don't overwrite our optimistic update)
      await queryClient.cancelQueries('policies');

      // Snapshot the previous value
      const previousPolicies =
        queryClient.getQueryData<{ count: number; data: Policy[] }>('policies');

      // Optimistically update to the new value
      if (previousPolicies) {
        queryClient.setQueryData<{ count: number; data: Policy[] }>('policies', {
          ...previousPolicies,
        });
      }
      return { previousPolicies };
    },
    onSuccess: ({ data }) => {
      addToast('Policy deleted correctly', { appearance: 'success', autoDismiss: true });
    },
    onError: (error: any, variable, context: any) => {
      addToast(error?.message as object, { appearance: 'error', autoDismiss: true });
      queryClient.setQueryData('policies', context.previousTodos);
    },
    onSettled: () => {
      queryClient.invalidateQueries('policies');
    },
  });
  const columns = useMemo(
    () => [
      {
        Header: 'Id',
        accessor: 'id',
        Cell: function cell({ value, row }: any) {
          return (
            <>
              <i
                className="mr-2 icon-zoomin3 pointer"
                title="Open policy details"
                onClick={() => handleOpenDetails(row.original)}
              ></i>{' '}
              {value}
            </>
          );
        },
      },
      {
        Header: 'Name',
        accessor: 'name',
      },
      {
        Header: 'Rules',
        accessor: '_count.policyRules',
        disableSortBy: true,
        Cell: function cell({ value, row }: any) {
          return (
            <ButtonStyled onClick={() => handleOpenRules(row)} variant="danger">
              View rules{' '}
              <BadgeStyled pill className="ml-2" variant="light">
                {value}
              </BadgeStyled>
              <span className="sr-only">Rules assigned</span>
            </ButtonStyled>
          );
        },
        className: 'text-center',
      },
      {
        Header: 'Priority',
        accessor: 'priority',
        className: 'text-center',
      },
      {
        Header: 'Created at',
        accessor: 'createdAt',
        Cell: function ({ value }: any) {
          return format(new Date(value), `yyyy-MM-dd - HH:mm aaa`);
        },
      },
      {
        Header: 'Updated at',
        accessor: 'updatedAt',
        Cell: function ({ value }: any) {
          return format(new Date(value), `yyyy-MM-dd - HH:mm aaa`);
        },
      },
      {
        Header: 'Actions',
        accessor: '',
        disableSortBy: true,
        width: '5%',
        Cell: function cell({ row: { values } }: any) {
          return (
            <ActionButtons
              handleOnEdit={() => handleOnEdit(values)}
              handleOnDelete={() => handleOnDelete(values)}
            />
          );
        },
      },
    ],
    [],
  );

  const handleOpenDetails = (row: any) => {
    setSelectedPolicy(row);
    setOpenDetails(true);
  };

  const handleOpenRules = (row: any) => {
    setSelectedPolicy(row.original);
    setOpenRule(true);
  };

  const handleOnEdit = ({ id }: Policy) => {
    history.push(`/policy/${id}`);
  };
  const handleOnDelete = (data: any) => {
    deleteMutation.mutate(data.id);
  };

  const openCreateForm = () => {
    history.push('/policy/0');
  };

  const menuActions: MenuActions[] = [
    {
      title: 'add',
      type: 'primary',
      handleOnClick: openCreateForm,
    },
  ];

  const handleOnCloseRulesModal = () => {
    setOpenRule(false);
  };

  useEffect(() => {
    const totalPages = query?.data?.count ? Math.ceil(query?.data?.count / pageSize) : 0;
    setPageCount(totalPages);
  }, [query?.data, setPageCount, pageSize]);

  return {
    policies,
    pageCount,
    handleOnFetch,
    columns,
    openRule,
    selectedPolicy,
    menuActions,
    openDetails,
    setOpenDetails,
    handleOnCloseRulesModal,
    history,
    ...query,
  };
};

export default usePolicies;
