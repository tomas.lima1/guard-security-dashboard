import { format } from 'date-fns';
import { useContext, useEffect, useMemo, useState } from 'react';
import { Badge } from 'react-bootstrap';
import { useMutation, useQuery, useQueryClient } from 'react-query';
import { useHistory } from 'react-router';
import deleteDepartment from '../api/deleteDepartment';
import getDepartments from '../api/getDepartments';
import ActionButtons from '../components/ActionButtons';
import { MenuActions } from '../components/CardBasic';
import { DepartmentContext } from '../contexts/DepartmentContext';
import { Department } from '../models/department';
import useTableOptions from './useTableOptions';
import useToastAlerts from './useToastAlerts';

const useDepartments = () => {
  const { addToast } = useToastAlerts();
  const history = useHistory();
  const [openDetails, setOpenDetails] = useState(false);
  const { pageIndex, pageSize, sort, globalFilter, handleOnFetch, setPageCount, pageCount } =
    useTableOptions();
  const { selectedDepartment, setSelectedDepartment } = useContext(DepartmentContext);

  const queryClient = useQueryClient();
  const query = useQuery(
    ['departments', pageIndex, pageSize, sort, globalFilter],
    () => getDepartments(pageIndex, pageSize, sort, globalFilter),
    {
      keepPreviousData: true,
      onError: ({ error }) =>
        addToast(`An erorr has occurred, please try again in few seconds!`, {
          appearance: 'error',
          autoDismiss: true,
        }),
    },
  );
  const deleteMutation = useMutation(deleteDepartment, {
    onMutate: async (departmentId) => {
      // Cancel any outgoing refetches (so they don't overwrite our optimistic update)
      await queryClient.cancelQueries('departments');

      // Snapshot the previous value
      const previousDepartments =
        queryClient.getQueryData<{ count: number; data: Department[] }>('departments');

      // Optimistically update to the new value
      if (previousDepartments) {
        queryClient.setQueryData<{ count: number; data: Department[] }>('departments', {
          ...previousDepartments,
        });
      }
      return { previousDepartments };
    },
    onSuccess: ({ data }) => {
      addToast('Department deleted correctly', { appearance: 'success', autoDismiss: true });
    },
    onError: (error: any, variable, context: any) => {
      addToast(error?.message as object, { appearance: 'error', autoDismiss: true });
      queryClient.setQueryData('departments', context.previousTodos);
    },
    onSettled: () => {
      queryClient.invalidateQueries('departments');
    },
  });

  const columns = useMemo(
    () => [
      {
        Header: 'Id',
        accessor: 'id',
        Cell: function cell({ value, row }: any) {
          return (
            <>
              <i
                className="mr-2 icon-zoomin3 pointer"
                title="Open department details"
                onClick={() => handleOpenDetails(row.original)}
              ></i>{' '}
              {value}
            </>
          );
        },
      },
      {
        Header: 'Name',
        accessor: 'name',
      },
      {
        Header: 'Enabled',
        accessor: 'enabled',
        Cell: function ({ value }: any) {
          return (
            <Badge variant={value ? 'success' : 'danger'}>{value ? 'Enabled' : 'Disabled'}</Badge>
          );
        },
      },
      {
        Header: 'Created by',
        accessor: 'metadata.createdBy.name',
        disableSortBy: true,
        Cell: function ({ value }: any) {
          return value || 'Undefined';
        },
      },
      {
        Header: 'Created at',
        accessor: 'createdAt',
        Cell: function ({ value }: any) {
          return format(new Date(value), `yyyy-MM-dd - HH:mm aaa`);
        },
      },
      {
        Header: 'Updated at',
        accessor: 'updatedAt',
        Cell: function ({ value }: any) {
          return format(new Date(value), `yyyy-MM-dd - HH:mm aaa`);
        },
      },
      {
        Header: 'Actions',
        accessor: '',
        disableSortBy: true,
        width: '5%',
        Cell: function ({ row: { values } }: any) {
          return (
            <ActionButtons
              handleOnEdit={() => handleOnEdit(values)}
              handleOnDelete={() => handleOnDelete(values)}
            />
          );
        },
      },
    ],
    [],
  );
  const openCreateForm = () => {
    history.push('/department/0');
  };
  const menuActions: MenuActions[] = [
    {
      title: 'add',
      type: 'primary',
      handleOnClick: openCreateForm,
    },
  ];

  const handleOpenDetails = (row: any) => {
    setSelectedDepartment(row);
    setOpenDetails(true);
  };

  const handleOnEdit = ({ id }: Department) => {
    history.push(`/department/${id}`);
  };
  const handleOnDelete = ({ id }: Department) => {
    deleteMutation.mutate(id);
  };

  useEffect(() => {
    const totalPages = query?.data?.count ? Math.ceil(query?.data?.count / pageSize) : 0;
    setPageCount(totalPages);
  }, [query?.data, setPageCount, pageSize]);

  return {
    pageCount,
    handleOnFetch,
    columns,
    openDetails,
    setOpenDetails,
    selectedDepartment,
    menuActions,
    history,
    ...query,
  };
};

export default useDepartments;
