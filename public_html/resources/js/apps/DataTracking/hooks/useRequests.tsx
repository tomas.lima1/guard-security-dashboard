import { format } from 'date-fns';
import { useContext, useEffect, useMemo, useState } from 'react';
import { useMutation, useQuery, useQueryClient } from 'react-query';
import cancelRequest from '../api/cancelRequest';
import getRequests from '../api/getRequest';
import resendRequest from '../api/resendRequest';
import ActionButtons from '../components/ActionButtons';
import BadgeWrapper from '../components/BadgeWrapper';
import { RequestContext } from '../contexts/RequestContext';
import { Request } from '../models/request';
import useTableOptions from './useTableOptions';
import useToastAlerts from './useToastAlerts';

const useRequests = () => {
  const { addToast } = useToastAlerts();
  const [openDetails, setOpenDetails] = useState(false);
  const { pageIndex, pageSize, sort, globalFilter, handleOnFetch, setPageCount, pageCount } =
    useTableOptions();
  const { setSelectedRequest, selectedRequest } = useContext(RequestContext);

  const query = useQuery(
    ['requests', pageIndex, pageSize, sort, globalFilter],
    () => getRequests(pageIndex, pageSize, sort, globalFilter),
    {
      keepPreviousData: true,
      onError: ({ error }) =>
        addToast(`An erorr has occurred, please try again in few seconds!`, {
          appearance: 'error',
          autoDismiss: true,
        }),
    },
  );
  const queryClient = useQueryClient();
  const cancelRequestMutation = useMutation(cancelRequest, {
    onMutate: async (requestId) => {
      // Cancel any outgoing refetches (so they don't overwrite our optimistic update)
      await queryClient.cancelQueries('requests');

      // Snapshot the previous value
      const previousRequests =
        queryClient.getQueryData<{ count: number; data: Request[] }>('requests');

      // Optimistically update to the new value
      if (previousRequests && previousRequests.count > 0) {
        const request = previousRequests.data.filter((x) => x.id === requestId)[0];

        queryClient.setQueryData<{ count: number; data: Request[] }>('requests', {
          ...previousRequests,
          data: [...previousRequests.data, { ...request, status: 'CANCELED' }],
        });
      }
      return { previousRequests };
    },
    onSuccess: () => {
      addToast('Request cancelled correctly', { appearance: 'warning', autoDismiss: true });
    },
    onError: (error: any) => {
      addToast(error?.message as object, { appearance: 'error', autoDismiss: true });
    },
    onSettled: () => {
      queryClient.invalidateQueries('requests');
    },
  });

  const resendRequestMutation = useMutation(resendRequest, {
    onMutate: async (requestId) => {
      // Cancel any outgoing refetches (so they don't overwrite our optimistic update)
      await queryClient.cancelQueries('requests');

      // Snapshot the previous value
      const previousRequests =
        queryClient.getQueryData<{ count: number; data: Request[] }>('requests');

      // Optimistically update to the new value
      if (previousRequests) {
        const request = previousRequests.data.find(({ id }) => id === requestId);

        if (request) {
          queryClient.setQueryData<{ count: number; data: Request[] }>('requests', {
            ...previousRequests,
            data: previousRequests.data.map((x) =>
              x.id === requestId ? { ...x, status: 'PENDING' } : x,
            ),
          });
        }
      }
      return { previousRequests };
    },
    onSuccess: () => {
      addToast('Request resend correctly', { appearance: 'info', autoDismiss: true });
    },
    onError: (error: any) => {
      addToast(error?.message as object, { appearance: 'error', autoDismiss: true });
    },
    onSettled: () => {
      queryClient.invalidateQueries('requests');
    },
  });

  const columns = useMemo(
    () => [
      {
        Header: 'Id',
        accessor: 'id',
        Cell: function cell({ value, row }: any) {
          return (
            <>
              <i
                className="mr-2 icon-zoomin3 pointer"
                title="Open policy details"
                onClick={() => handleOpenDetails(row.original)}
              ></i>{' '}
              {value}
            </>
          );
        },
      },
      {
        Header: 'Status',
        accessor: 'status',
        Cell: function cell({ value, row }: any) {
          return <BadgeWrapper status={value} />;
        },
        className: 'text-center',
      },
      {
        Header: 'Created by',
        accessor: 'metadata.createdBy.name',
        disableSortBy: true,
      },
      {
        Header: 'Policy Rule',
        accessor: 'policyRule.name',
        disableSortBy: true,
      },
      {
        Header: 'From',
        accessor: 'requestor.name',
        description: 'Department',
        disableSortBy: true,
      },
      {
        Header: 'To',
        accessor: 'addressedTo.name',
        description: 'Department',
        disableSortBy: true,
      },
      {
        Header: 'Created at',
        accessor: 'createdAt',
        Cell: function ({ value }: any) {
          return format(new Date(value), `yyyy-MM-dd - HH:mm aaa`);
        },
      },
      {
        Header: 'Actions',
        accessor: '',
        disableSortBy: true,
        width: '5%',
        Cell: function cell({ row }: any) {
          const hasPatientAnswer =
            row.original.status === 'DENIED' || row.original.status === 'APPROVED';

          if (hasPatientAnswer) {
            return null;
          }

          const actionProps = {
            handleOnResend: () => handleOnResend(row),
            handleOnCancel: () => handleOnCancel(row),
          };
          return <ActionButtons {...actionProps} />;
        },
      },
    ],
    [],
  );

  const handleOpenDetails = (row: any) => {
    setSelectedRequest(row);
    setOpenDetails(true);
  };

  const handleOnCancel = ({ values }: any) => {
    // console.log(values);
    cancelRequestMutation.mutate(values.id);
  };
  const handleOnResend = ({ values }: any) => {
    resendRequestMutation.mutate(values.id);
  };

  useEffect(() => {
    const totalPages = query?.data?.count ? Math.ceil(query.data.count / pageSize) : 0;
    setPageCount(totalPages);
  }, [query?.data, setPageCount, pageSize]);

  return {
    pageCount,
    handleOnFetch,
    columns,
    setOpenDetails,
    openDetails,
    selectedRequest,
    ...query,
  };
};

export default useRequests;
