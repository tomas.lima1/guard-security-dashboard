// import { useEffect } from 'react'
import { useEffect } from 'react';
import {
  useGlobalFilter,
  usePagination,
  useSortBy,
  useTable,
  TableInstance,
} from 'react-table';
import { HandleFetchProps } from './useTableOptions';
// import { NumberParam, StringParam, withDefault } from 'serialize-query-params'
// import { useQueryParams } from 'use-query-params'
// import sortToString from '../helpers/sortToString'

type Props = {
  data: Array<any>;
  columns: Array<any>;
  pageCount: number;
  defaultPageSize?: number;
  manualSortBy?: boolean;
  manualPagination?: boolean;
  manualGlobalFilter?: boolean;
  handleOnFetch?: (props: HandleFetchProps) => void;
};

export type TableState = {
  pageSize: number;
  pageIndex: number;
  sortBy: Array<any>;
  globalFilter: string;
};

type TableInstanceType = TableInstance<object> & {
  page: any;
  state: TableState;
};

const useReactTable = ({
  // defaultPageSize = 5,
  data,
  columns,
  pageCount,
  manualGlobalFilter = true,
  manualPagination = true,
  manualSortBy = true,
  handleOnFetch,
}: Props) => {
  // const [query, setQuery] = useQueryParams({
  //   q: withDefault(StringParam, ''),
  //   page: withDefault(NumberParam, 1),
  //   size: withDefault(NumberParam, defaultPageSize),
  //   sort: withDefault(StringParam, ''),
  // })

  const table = useTable(
    {
      initialState: {
        // globalFilter: query.q,
        // pageSize: isNaN(query.size) ? defaultPageSize : query.size,
        // pageIndex: isNaN(query.page) ? 0 : query.page - 1,
        // sortBy: [
        //   {
        //     id:
        //       query.sort.charAt(0) === '-'
        //         ? query.sort.substring(1)
        //         : query.sort,
        //     desc: query.sort.charAt(0) === '-',
        //   },
        // ],
      },
      columns,
      data,
      manualSortBy,
      manualPagination,
      manualGlobalFilter,
      autoResetPage: true,
      autoResetSortBy: true,
      pageCount,
    } as any,
    useGlobalFilter,
    useSortBy,
    usePagination,
  ) as TableInstanceType;

  const { pageIndex, pageSize, sortBy, globalFilter } = table.state;

  useEffect(() => {
    handleOnFetch &&
      handleOnFetch({ sortBy, pageSize, pageIndex, globalFilter });
  }, [handleOnFetch, sortBy, pageSize, pageIndex, globalFilter]);

  // const { pageSize, pageIndex, sortBy, globalFilter } = table.state

  // // Set query global filter
  // useEffect(() => {
  //   setQuery({ q: globalFilter })
  // }, [globalFilter, setQuery])

  // // Set query pageSize
  // useEffect(() => {
  //   setQuery({ size: pageSize })
  // }, [pageSize, setQuery])

  // // Set query page
  // useEffect(() => {
  //   setQuery({ page: pageIndex + 1 })
  // }, [setQuery, pageIndex])

  // // Set query sortBy
  // useEffect(() => {
  //   setQuery({ sort: sortToString(sortBy) })
  // }, [query.sort, setQuery, sortBy])

  return table;
};

export default useReactTable;
