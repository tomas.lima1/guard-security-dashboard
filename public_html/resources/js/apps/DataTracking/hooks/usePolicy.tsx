import { yupResolver } from '@hookform/resolvers/yup';
import { useEffect, useMemo, useState } from 'react';
import { useForm, useFormState } from 'react-hook-form';
import { useMutation, useQuery, useQueryClient } from 'react-query';
import { useHistory, useParams } from 'react-router-dom';
import * as yup from 'yup';
import createPolicy from '../api/createPolicy';
import getPolicy from '../api/getPolicy';
import updatePolicy from '../api/updatePolicy';
import { Policy } from '../models/policy';
import useToastAlerts from './useToastAlerts';

const PolicySchema = yup.object().shape({
  id: yup.string().when('auto', { is: false, then: yup.string().required() }),
  auto: yup.boolean(),
  name: yup.string().required(),
  path: yup.string().required(),
  departmentId: yup.string().required(),
  description: yup.string(),
  priority: yup.number().positive().integer().required(),
});

interface FormType extends Policy {
  auto: boolean;
}

const usePolicy = () => {
  const { addToast } = useToastAlerts();
  const history = useHistory();
  const { id } = useParams<{ id: string }>();
  const [isEdit] = useState(id !== '0');
  const queryClient = useQueryClient();
  const query = useQuery([`policy-${id}`, id], () => getPolicy(id), {
    enabled: isEdit,
    keepPreviousData: true,
  });
  const { data } = query;
  const policy = useMemo(() => data?.data, [data]);

  const formMethods = useForm<FormType>({
    defaultValues: {
      id: '',
      name: '',
      path: '',
      departmentId: '',
      description: '',
      priority: 0,
    },
    resolver: yupResolver(PolicySchema),
  });

  const { control, setValue } = formMethods;
  const state = useFormState({ control });

  useEffect(() => {
    if (policy) {
      setValue('id', policy.id);
      setValue('name', policy.name);
      setValue('path', policy.path);
      setValue('departmentId', policy.departmentId);
      setValue('description', policy.description);
      setValue('enabled', policy.enabled);
      setValue('priority', policy.priority);
    }
  }, [policy]);

  const createMutation = useMutation(createPolicy, {
    onMutate: async (newPolicy) => {
      // Cancel any outgoing refetches (so they don't overwrite our optimistic update)
      await queryClient.cancelQueries('policies');

      // Snapshot the previous value
      const previousPolicy =
        queryClient.getQueryData<{ count: number; data: Policy[] }>('policies');

      // Optimistically update to the new value
      if (previousPolicy) {
        queryClient.setQueryData<{ count: number; data: Policy[] }>('policies', {
          ...previousPolicy,
          data: [...previousPolicy.data, newPolicy],
        });
      }
      return { previousPolicy };
    },
    onSuccess: ({ data }) => {
      addToast('Policy created correctly', { appearance: 'success', autoDismiss: true });
      history.replace(`/policy/${data.id}`);
    },
    onError: (error: any, variable, context: any) => {
      addToast(error?.message as object, { appearance: 'error', autoDismiss: true });
      queryClient.setQueryData('policies', context.previousTodos);
    },
    onSettled: () => {
      queryClient.invalidateQueries('policies');
    },
  });
  const updateMutation = useMutation(updatePolicy, {
    onMutate: async (newPolicy) => {
      // Cancel any outgoing refetches (so they don't overwrite our optimistic update)
      await queryClient.cancelQueries('policies');

      // Snapshot the previous value
      const previousPolicy =
        queryClient.getQueryData<{ count: number; data: Policy[] }>('policies');

      // Optimistically update to the new value
      if (previousPolicy) {
        queryClient.setQueryData<{ count: number; data: Policy[] }>('policies', {
          ...previousPolicy,
          data: [...previousPolicy.data, newPolicy],
        });
      }
      return { previousPolicy };
    },
    onSuccess: ({ data }) => {
      addToast('Policy updated correctly', { appearance: 'success', autoDismiss: true });
    },
    onError: (error: any, variable, context: any) => {
      addToast(error?.message as object, { appearance: 'error', autoDismiss: true });
      queryClient.setQueryData('policies', context.previousTodos);
    },
    onSettled: () => {
      queryClient.invalidateQueries('policies');
    },
  });

  const onSubmit = (data: FormType) => {
    const policyId = id;
    if (isEdit) {
      const { id, auto, ...form } = data;
      updateMutation.mutate({ id: policyId, ...form });
    } else {
      const { auto, ...form } = data;
      createMutation.mutate(form);
    }
  };

  return {
    isEdit,
    id,
    onSubmit,
    history,
    ...formMethods,
    state,
    createMutation,
    updateMutation,
  };
};

export default usePolicy;
