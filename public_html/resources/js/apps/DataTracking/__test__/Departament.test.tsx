import { mount } from 'enzyme';
import { MemoryRouter } from 'react-router-dom';
import { QueryParamProvider } from 'use-query-params';
import Department from '../Department';

const mockAddToast = jest.fn();
jest.mock('../hooks/useToastAlerts', () => {
  return jest.fn(() => ({
    addToast: mockAddToast,
  }));
});

jest.mock('crypto-random-string', () => jest.fn());

const renderWrapper = () => {
  return mount(
    <QueryParamProvider>
      <MemoryRouter>
        <Department />
      </MemoryRouter>
    </QueryParamProvider>,
  );
};

describe('<Departments/> screen', () => {
  test('should match the snapshot', () => {
    const wrapper = renderWrapper();
    expect(wrapper).toMatchSnapshot();
  });

  test('should be the rigth site', () => {
    const wrapper = renderWrapper();
    const title = wrapper.find('.page-title').text();

    expect(title).toContain('Department');
  });
});
