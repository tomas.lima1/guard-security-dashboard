import { mount } from 'enzyme';
import { MemoryRouter } from 'react-router-dom';
import { QueryParamProvider } from 'use-query-params';
import Policies from '../Policies';

const mockAddToast = jest.fn();
jest.mock('../hooks/useToastAlerts', () => {
  return jest.fn(() => ({
    addToast: mockAddToast,
  }));
});
const renderWrapper = () => {
  return mount(
    <QueryParamProvider>
      <MemoryRouter>
        <Policies />
      </MemoryRouter>
    </QueryParamProvider>,
  );
};

describe('<Policies/> screen', () => {
  test('should match the snapshot', () => {
    const wrapper = renderWrapper();
    expect(wrapper).toMatchSnapshot();
  });

  test('should be the rigth site', () => {
    const wrapper = renderWrapper();
    const title = wrapper.find('.page-title').text();

    expect(title).toContain('Policies');
  });
});
