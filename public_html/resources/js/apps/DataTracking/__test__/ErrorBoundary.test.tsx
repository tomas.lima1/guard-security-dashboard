import { mount } from 'enzyme'
import { MemoryRouter } from 'react-router-dom'
import { QueryParamProvider } from 'use-query-params';
import ErrorBoundary from '../ErrorBoundary'


const Something = () => null;
const renderWrapper = () => {
  const wrapper = mount(
    <QueryParamProvider>
      <MemoryRouter>
        <ErrorBoundary>
          <Something />
        </ErrorBoundary>
      </MemoryRouter>
    </QueryParamProvider>
  )
  const error = new Error('test');
  wrapper.find(Something).simulateError(error);
  return wrapper
}

describe('<ErrorBoundary/> screen', () => {
  test('should match the snapshot', () => {
    const wrapper = renderWrapper()
    expect(wrapper).toMatchSnapshot()
  })

  test('should render the error title', () => {
    const wrapper = renderWrapper()
    const title = wrapper.find('.card-title').text()
    expect(title).toContain('Something went wrong')
  })

  test('should show the error message', () => {
    const wrapper = renderWrapper()
    const errorMessage = wrapper.find('.card-body').text()
    expect(errorMessage).toContain('test')
  })
  
})
