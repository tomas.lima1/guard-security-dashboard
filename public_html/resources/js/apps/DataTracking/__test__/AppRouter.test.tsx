import { render, screen } from '@testing-library/react';
import { AppRouter } from '../AppRouter';
import { BrowserRouter } from 'react-router-dom';
import { QueryParamProvider } from 'use-query-params';

const mockAddToast = jest.fn();
jest.mock('../../../shared/hooks/useToastAlerts', () => {
  return jest.fn(() => ({
    addToast: mockAddToast,
  }));
});
jest.mock('crypto-random-string', () => jest.fn());

const renderWithRouter = (ui: any, { route = '/data-trace' } = {}) => {
  window.history.pushState({}, 'Test page', route);
  return render(<QueryParamProvider>{ui}</QueryParamProvider>, {
    wrapper: BrowserRouter,
  });
};

describe('<AppRouter />', () => {
  test('should route to the default page', () => {
    renderWithRouter(<AppRouter />);
    expect(screen.getByText(/Example subtitle/)).toBeInTheDocument();
  });

  test('should route to flows page', () => {
    renderWithRouter(<AppRouter />, { route: '/data-trace/departments' });
    expect(screen.getAllByText(/Departments/).length).toBeGreaterThanOrEqual(1);
  });

  test('should route to request page if route does not exists', () => {
    renderWithRouter(<AppRouter />, { route: '/data-trace/BAD-ROUTE' });
    expect(screen.getAllByText(/Requests/).length).toBeGreaterThanOrEqual(1);
  });
});
