import { mount } from 'enzyme';
import Requests from '../Requests';
import { MemoryRouter } from 'react-router-dom';
import { QueryParamProvider } from 'use-query-params';

const mockAddToast = jest.fn();
jest.mock('../hooks/useToastAlerts', () => {
  return jest.fn(() => ({
    addToast: mockAddToast,
  }));
});

const renderWrapper = () => {
  return mount(
    <QueryParamProvider>
      <MemoryRouter>
        <Requests />
      </MemoryRouter>
    </QueryParamProvider>,
  );
};

describe('<Request/> screen', () => {
  test('should match the snapshot', () => {
    const wrapper = renderWrapper();
    expect(wrapper).toMatchSnapshot();
  });

  test('should be the rigth site', () => {
    const wrapper = renderWrapper();
    const title = wrapper.find('.page-title').text();

    expect(title).toContain('Patient requests');
  });

  test('should contain a table', () => {
    const wrapper = renderWrapper();
    const table = wrapper.find('table');

    expect(table.prop('className')).toContain('dataTable');
  });
});
