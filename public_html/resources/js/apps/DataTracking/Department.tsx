import { Button, Col, Container, Form, FormControl, InputGroup, Row } from 'react-bootstrap';
import Header from './components/Header';
import CardBasic from './components/CardBasic';
import useDepartment from './hooks/useDepartment';
import FormControlFeedback from './components/FormControlFeedback';
import InputCustomForId from './components/form/InputCustomForId';
import { FormProvider } from 'react-hook-form';
import { InputEnableSwitch } from './components/form/InputEnableSwitch';
const Department = () => {
  const methods = useDepartment();
  const {
    isEdit,
    id,
    handleSubmit,
    onSubmit,
    register,
    history,
    createMutation,
    updateMutation,
    state: { errors },
  } = methods;

  return (
    <>
      <Header
        sectionTitle="User Data Traceability"
        headerSubtitle="Departments endpoints"
        currentRoute={!isEdit ? 'Create department' : 'Edit department'}
      />
      <CardBasic
        title={`${
          isEdit ? `Department ID: <strong>${id}</strong>` : 'Add a new department endpoint'
        }`}
      >
        <Container>
          <Row className="justify-content-center">
            <Col lg="8">
              <FormProvider {...methods}>
                <Form onSubmit={handleSubmit(onSubmit)}>
                  <Form.Group as={Row} controlId="id">
                    <Form.Label column sm="3">
                      Department Id
                    </Form.Label>
                    <Col sm="9">
                      <InputCustomForId disabled={isEdit} name="id" />
                    </Col>
                  </Form.Group>
                  <Form.Group as={Row} controlId="name">
                    <Form.Label column sm="3">
                      Name
                    </Form.Label>
                    <Col sm="9">
                      <Form.Control
                        type="text"
                        placeholder="(*) A name for your department"
                        {...register('name')}
                        isInvalid={!!errors.name}
                      />
                      <FormControlFeedback errors={errors} name="name" />
                    </Col>
                  </Form.Group>
                  <Form.Group as={Row} controlId="endpoint">
                    <label htmlFor="endpoint" className="col-sm-3">
                      Endpoint
                    </label>
                    <Col sm="9">
                      <InputGroup hasValidation>
                        <FormControl
                          aria-describedby="endpoint"
                          placeholder="(*) http://domain.tld/to/endpoint"
                          {...register('endpoint')}
                          isInvalid={!!errors.endpoint}
                        />
                        <InputGroup.Append>
                          <Button variant="ligth">Test connection</Button>
                        </InputGroup.Append>
                        <FormControlFeedback errors={errors} name="endpoint" />
                      </InputGroup>
                    </Col>
                  </Form.Group>
                  <Form.Group as={Row} controlId="description">
                    <Form.Label column sm="3">
                      Description
                    </Form.Label>
                    <Col sm="9">
                      <Form.Control
                        as="textarea"
                        aria-describedby="description"
                        rows={2}
                        placeholder="Describe the purpose of this department"
                        {...register('description')}
                      />
                    </Col>
                  </Form.Group>
                  <Col sm={{ span: 9, offset: 3 }}>
                    <Form.Group as={Row} controlId="enabled">
                      <InputEnableSwitch name="enabled" label="Department status" />
                    </Form.Group>
                  </Col>
                  <Col sm={{ span: 9, offset: 3 }}>
                    <Form.Group as={Row} controlId="enable">
                      <Col>
                        <Button
                          title="Go back to the preview page visited"
                          block
                          variant="outline-secondary"
                          type="button"
                          onClick={() => history.goBack()}
                        >
                          Back
                        </Button>
                      </Col>
                      <Col>
                        <Button
                          title={!isEdit ? 'Create a new department' : 'Save changes'}
                          block
                          variant={!isEdit ? 'success' : 'info'}
                          type="submit"
                        >
                          {createMutation.isLoading || updateMutation.isLoading
                            ? 'loading...'
                            : !isEdit
                            ? 'Create'
                            : 'Edit'}
                        </Button>
                      </Col>
                    </Form.Group>
                  </Col>
                </Form>
              </FormProvider>
            </Col>
          </Row>
        </Container>
      </CardBasic>
    </>
  );
};

export default Department;
