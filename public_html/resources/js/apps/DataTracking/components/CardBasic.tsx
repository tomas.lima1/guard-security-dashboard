import { useState, ReactNode } from 'react';

type Props = {
  title?: string;
  actions?: MenuActions[];
  children: ReactNode;
};
export interface MenuActions {
  title: string;
  type: 'primary' | 'secondary' | 'info' | 'danger' | 'warning';
  handleOnClick?: () => void;
}

const CardBasic = ({ title, actions = [], children }: Props) => {
  const [cardCollapsed, setCardCollapsed] = useState(false);
  return (
    <>
      <div className={`card ${cardCollapsed ? 'card-collapsed' : ''}`}>
        {title && (
          <div className="bg-transparent card-header header-elements-inline">
            <h5
              className="card-title"
              dangerouslySetInnerHTML={{ __html: title }}
            ></h5>
            <div className="header-elements">
              <div className="list-icons">
                {actions.map((action) => (
                  <a
                    key={action.title}
                    onClick={action.handleOnClick}
                    className={`btn btn-${action.type} list-icons-item text-white pointer`}
                  >
                    {action.title}
                  </a>
                ))}
                <a
                  className={`list-icons-item ml-4 disable-select ${
                    cardCollapsed ? 'rotate-180' : ''
                  }`}
                  data-action="collapse"
                  onClick={() => setCardCollapsed(!cardCollapsed)}
                ></a>
              </div>
            </div>
          </div>
        )}
        <div className={`card-body ${cardCollapsed ? 'd-none' : ''}`}>
          {children}
        </div>
      </div>
    </>
  );
};

export default CardBasic;
