import cryptoRandomString from 'crypto-random-string';
import { Button, Form, FormControl, InputGroup, Row } from 'react-bootstrap';
import { useFormContext, useFormState } from 'react-hook-form';
import FormControlFeedback from '../FormControlFeedback';
import PropTypes from 'prop-types';

type Props = {
  name: string;
  randomType?:
    | 'hex'
    | 'base64'
    | 'url-safe'
    | 'numeric'
    | 'distinguishable'
    | 'ascii-printable'
    | 'alphanumeric';
  randomLength?: number;
  disabled?: boolean;
};

const InputCustomForId = ({
  name,
  randomLength = 12,
  randomType = 'alphanumeric',
  disabled,
}: Props) => {
  const { register, watch, control, setValue, unregister, clearErrors } = useFormContext();

  const { errors } = useFormState({ control });

  const handleGenerateNewPolicyId = () => {
    setValue(name, cryptoRandomString({ length: randomLength, type: randomType }));
  };

  const handleAutoCheckbox = (e: any) => {
    setValue('auto', e.target.checked);
    setValue(name, '');
    unregister(name);
    clearErrors(name);
  };

  return (
    <InputGroup hasValidation>
      <FormControl
        placeholder={watch('auto') ? `Auto generated Id by server` : '(*) ID'}
        aria-label="ID"
        aria-describedby={name}
        disabled={watch('auto') || disabled}
        {...register(name)}
        isInvalid={!!errors[name]}
      />
      <InputGroup.Append>
        <Button
          variant="ligth"
          disabled={watch('auto') || disabled}
          onClick={handleGenerateNewPolicyId}
        >
          <i className="mr-2 icon-sync"></i>
          Generate
        </Button>
      </InputGroup.Append>
      <InputGroup.Append>
        <Form.Group as={Row} controlId="auto">
          <Form.Check
            {...register('auto')}
            disabled={disabled}
            onChange={handleAutoCheckbox}
            label="Auto"
          />
        </Form.Group>
      </InputGroup.Append>
      <FormControlFeedback errors={errors} name={name} />
    </InputGroup>
  );
};

InputCustomForId.propTypes = {
  name: PropTypes.string.isRequired,
  randomLength: PropTypes.number,
  randomType: PropTypes.string,
};

export default InputCustomForId;
