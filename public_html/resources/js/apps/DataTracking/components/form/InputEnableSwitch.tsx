import { useFormContext } from 'react-hook-form';
import styled from 'styled-components';

type Props = {
  name: string;
  enable?: boolean;
  label: string;
};

const CheckBoxWrapper = styled.div`
  position: relative;
`;
const CheckBoxLabel = styled.label`
  position: absolute;
  top: 0;
  left: 0;
  width: 42px;
  border-radius: 15px;
  background: #bebebe;
  cursor: pointer;
  &::after {
    content: '';
    display: block;
    border-radius: 50%;
    width: 18px;
    height: 18px;
    margin: 3px;
    background: #ffffff;
    box-shadow: 1px 3px 3px 1px rgba(0, 0, 0, 0.2);
    transition: 0.2s;
  }
`;
const CheckBox = styled.input`
  opacity: 0;
  z-index: 1;
  border-radius: 15px;
  width: 42px;
  height: 26px;
  &:focus + label {
    border: 1px solid #adaaaa;
    background: #d3d1d1;
    width: 43px;
  }
  &:checked + ${CheckBoxLabel} {
    background: #4fbe79;
    &::after {
      content: '';
      display: block;
      border-radius: 50%;
      width: 18px;
      height: 18px;
      margin-left: 21px;
      transition: 0.2s;
    }
  }
`;
const Label = styled.label`
  display: inline-flex;
  align-self: center;
  margin-left: 1em;
  user-select: none;
  cursor: pointer;
`;

export const InputEnableSwitch = ({ name, label }: Props) => {
  const { register } = useFormContext();

  return (
    <>
      <CheckBoxWrapper>
        <CheckBox id="checkbox" type="checkbox" {...register(name)} />
        <CheckBoxLabel htmlFor="checkbox" />
      </CheckBoxWrapper>
      <Label htmlFor="checkbox">{label}</Label>
    </>
  );
};

export default InputEnableSwitch;
