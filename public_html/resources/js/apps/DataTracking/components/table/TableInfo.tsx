type Props = {
  state: {
    pageIndex: number;
    pageSize: number;
  };
  pageOptions: Array<any>;
};

export default function TableInfo({
  state: { pageIndex, pageSize },
  pageOptions,
}: Props) {
  return (
    <div className="dataTables_info">
      Showing {pageIndex * pageSize + 1} to {(pageIndex + 1) * pageSize} of{' '}
      {pageOptions.length} entries
    </div>
  );
}
