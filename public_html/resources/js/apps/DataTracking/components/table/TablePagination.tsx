import styled from 'styled-components';

const GoToPage = styled.span`
  margin: 0 0.5em;

  input {
    width: 80px;
    text-align: center;
    padding: 0.5em;
    font-size: 0.9rem;
  }
`;

type Props = {
  state: {
    pageIndex: number;
  };
  canPreviousPage: boolean;
  pageOptions: Array<any>;
  canNextPage: boolean;
  gotoPage: (x: number) => void;
  previousPage: () => void;
  nextPage: () => void;
};

const TablePagination = ({
  state: { pageIndex },
  gotoPage,
  canPreviousPage,
  previousPage,
  nextPage,
  pageOptions,
  canNextPage,
}: Props) => {
  return (
    <div className="dataTables_paginate paging_simple">
      <a
        onClick={() => gotoPage(0)}
        className={`paginate_button previous disable-select ${
          !canPreviousPage && 'disabled'
        }`}
      >
        &laquo;
      </a>
      <a
        onClick={previousPage}
        className={`paginate_button previous disable-select ${
          !canPreviousPage && 'disabled'
        }`}
      >
        ← Prev
      </a>
      <GoToPage>
        <input
          type="number"
          className="ml-1"
          onChange={(e) => {
            const page = e.target.value ? Number(e.target.value) - 1 : 0;
            gotoPage(page);
          }}
          value={pageIndex + 1}
        />
      </GoToPage>
      <a
        onClick={nextPage}
        className={`paginate_button next disable-select ${
          !canNextPage && 'disabled'
        }`}
      >
        Next →
      </a>
      <a
        onClick={() => gotoPage(pageOptions.length - 1)}
        className={`paginate_button next disable-select ${
          !canNextPage && 'disabled'
        }`}
      >
        &raquo;
      </a>
    </div>
  );
};

export default TablePagination;
