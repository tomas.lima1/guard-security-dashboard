import TableInfo from './TableInfo';
import TablePagination from './TablePagination';

const TableFooter = (props: any) => {
  return (
    <div className="datatable-footer">
      <TableInfo {...props} />
      <TablePagination {...props} />
    </div>
  );
};

export default TableFooter;
