import { Form } from 'react-bootstrap';
import styled from 'styled-components';

const Select = styled(Form.Control)`
  font-size: 0.9rem;
  margin-left: 0.6em;
  text-align: center;
  padding: 0.5em;
`;

type Props = {
  state: { pageSize: number };
  setPageSize: (x: number) => void;
  pageSizeOptions: Array<number>;
};

const TableSize = ({
  state: { pageSize },
  setPageSize,
  pageSizeOptions = [5, 10, 25, 50, 100],
}: Props) => {
  return (
    <div className="form-group row">
      <div className="input-group">
        <span className="input-group-text">Show: </span>
        <Select
          as="select"
          className="form-control"
          value={pageSize}
          onChange={(e: any) => setPageSize(+e.target.value)}
          style={{
            width: 'fit-content',
          }}
        >
          {pageSizeOptions.map((opt) => (
            <option value={opt} key={opt}>
              {opt}
            </option>
          ))}
        </Select>
      </div>
    </div>
  );
};

export default TableSize;
