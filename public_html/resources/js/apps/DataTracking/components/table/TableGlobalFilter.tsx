import { useState } from 'react';
import { useAsyncDebounce } from 'react-table';

type TableGlobalFilter = {
  state: {
    globalFilter: string;
  };
  setGlobalFilter: (x: string | undefined) => void;
};

const TableGlobalFilter = ({
  state: { globalFilter },
  setGlobalFilter,
}: TableGlobalFilter) => {
  const [filter, setFilter] = useState(globalFilter);
  const onChange = useAsyncDebounce((value) => {
    setGlobalFilter(value || undefined);
  }, 200);

  return (
    <div className="form-group-feedback form-group-feedback-right">
      <input
        className="form-control"
        name="filter"
        value={filter || ''}
        onChange={(e) => {
          setFilter(e.target.value);
          onChange(e.target.value);
        }}
        type="search"
        placeholder="Search"
      />
      <div className="form-control-feedback">
        <i className="icon-search4 font-size-base text-muted"></i>
      </div>
    </div>
  );
};

export default TableGlobalFilter;
