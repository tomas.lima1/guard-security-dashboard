import TableGlobalFilter from './TableGlobalFilter';
import TableSize from './TableSize';

type Props = {
  enableFilter?: boolean;
  enablePagination?: boolean;
  [key: string]: any;
};

const TableHeader = ({
  enableFilter = true,
  enablePagination = true,
  ...props
}: Props) => {
  return (
    <div className="table-header">
      {enableFilter && <TableGlobalFilter {...(props as any)} />}
      {enablePagination && <TableSize {...(props as any)} />}
    </div>
  );
};

export default TableHeader;
