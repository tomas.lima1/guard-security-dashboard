import BTable from 'react-bootstrap/Table';
import styled from 'styled-components';
import useReactTable from '../../hooks/useReactTable';
import { HandleFetchProps } from '../../hooks/useTableOptions';
import TableFooter from './TableFooter';
import TableHeader from './TableHeader';

const Loading = styled.div`
  position: absolute;
  z-index: 2;
  right: 0;
  left: 0;
  text-align: center;
  margin: 10px;

  i {
    font-size: 2em;
  }
`;

type Props = {
  data: Array<any>;
  columns: Array<any>;
  isLoading: boolean;
  pageCount: number;
  pageSizeOptions: Array<number>;
  handleOnFetch?: (props: HandleFetchProps) => void;
  defaultPageSize?: number;
  manualGlobalFilter?: boolean;
  manualPagination?: boolean;
  manualSortBy?: boolean;
};

const Table = ({
  data = [],
  columns = [],
  isLoading = false,
  pageSizeOptions,
  handleOnFetch,
  ...props
}: Props) => {
  const { headerGroups, getTableProps, page, prepareRow, getTableBodyProps, ...tableProps } =
    useReactTable({ data, columns, handleOnFetch, ...props });

  if (!columns.length) {
    return (
      <div style={{ textAlign: 'center' }}>
        <p>💬 No data is available.</p>
      </div>
    );
  }

  return (
    <>
      <TableHeader pageSizeOptions={pageSizeOptions} {...tableProps} />

      <BTable className="dataTable loading table-hover" responsive {...getTableProps()}>
        <thead>
          {headerGroups.map(({ getHeaderGroupProps, headers }: any) => (
            <tr className="disable-select" {...getHeaderGroupProps()}>
              {headers.map(
                ({
                  getHeaderProps,
                  getSortByToggleProps,
                  isSorted,
                  isSortedDesc,
                  canSort,
                  width,
                  render,
                  description,
                }: any) => (
                  <th
                    {...getHeaderProps(getSortByToggleProps())}
                    style={{ width }}
                    className={`${canSort && !isSorted && 'sorting'} ${isSorted && 'sorting_asc'} ${
                      isSortedDesc && 'sorting_desc'
                    }`}
                  >
                    {render('Header')}
                    {description && <small className="d-block"> {description}</small>}
                  </th>
                ),
              )}
            </tr>
          ))}
        </thead>
        <tbody {...getTableBodyProps()}>
          {page.map((row: any) => {
            prepareRow(row);
            return (
              <tr style={{ whiteSpace: 'nowrap' }} {...row.getRowProps()}>
                {row.cells.map(({ column, getCellProps, render }: any) => (
                  <td className={`${column.className}`} {...getCellProps()}>
                    {render('Cell')}
                  </td>
                ))}
              </tr>
            );
          })}
        </tbody>
      </BTable>

      {isLoading && (
        <Loading>
          <i className="mr-2 icon-spinner6 spinner"></i>
        </Loading>
      )}

      <TableFooter {...tableProps} />
    </>
  );
};

export default Table;
