import { ErrorMessage } from '@hookform/error-message';
import { Form } from 'react-bootstrap';
import { DeepMap, FieldError, FieldValues } from 'react-hook-form';

type FeedbackProps = {
  errors: DeepMap<FieldValues, FieldError> | undefined;
  name: string;
};

const FormControlFeedback = ({ errors, name }: FeedbackProps) => {
  return (
    <ErrorMessage
      errors={errors}
      name={name}
      render={({ message }) => (
        <Form.Control.Feedback type="invalid">{message}</Form.Control.Feedback>
      )}
    />
  );
};

export default FormControlFeedback;
