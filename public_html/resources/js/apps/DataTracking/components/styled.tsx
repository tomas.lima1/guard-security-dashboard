import styled from 'styled-components';

export const GlobalStyles = styled.div`
  .pointer {
    cursor: pointer !important;
  }

  .disable-select {
    user-select: none; /* supported by Chrome and Opera */
    -webkit-user-select: none; /* Safari */
    -khtml-user-select: none; /* Konqueror HTML */
    -moz-user-select: none; /* Firefox */
    -ms-user-select: none; /* Internet Explorer/Edge */
  }

  .dataTable {
    overflow: hidden;
  }

  .navigationIcons {
    font-size: 2em !important;
  }

  .active-link {
    border: 1px solid rgb(209, 209, 209) !important;
    transition: border 0.9s !important;
  }

  .table-header {
    display: flex;
    justify-content: space-between;
    flex-wrap: wrap;

    @media (max-width: 350px) {
      justify-content: center;
    }
  }
`;
