import { Dropdown } from 'react-bootstrap';

type Props = {
  handleOnEdit?: (e: any) => void;
  handleOnDelete?: (e: any) => void;
  handleOnResend?: (e: any) => void;
  handleOnCancel?: (e: any) => void;
};

const ActionButtons = ({ handleOnEdit, handleOnDelete, handleOnResend, handleOnCancel }: Props) => (
  <Dropdown drop="right">
    <Dropdown.Toggle as="a" className="list-icons-item">
      <i className="icon-menu7 pointer"></i>
    </Dropdown.Toggle>
    <Dropdown.Menu className="disable-select">
      {handleOnEdit && (
        <Dropdown.Item onClick={handleOnEdit}>
          <i className="mi-mode-edit"></i>Edit
        </Dropdown.Item>
      )}
      {handleOnResend && (
        <Dropdown.Item onClick={handleOnResend}>
          <i className="mi-autorenew"></i>Resend
        </Dropdown.Item>
      )}
      {handleOnCancel && (
        <Dropdown.Item onClick={handleOnCancel}>
          <i className="mi-cancel"></i>Cancel
        </Dropdown.Item>
      )}
      {handleOnDelete && (
        <Dropdown.Item onClick={handleOnDelete}>
          <i className="mi-delete"></i>Delete
        </Dropdown.Item>
      )}
    </Dropdown.Menu>
  </Dropdown>
);

export default ActionButtons;
