import PropTypes from 'prop-types';
import { Breadcrumb } from 'react-bootstrap';
import { NavLink } from 'react-router-dom';
import styled from 'styled-components';
import { useState } from 'react';
import { MENU_ELEMENTS } from '../config/data-tracking.config';

const BreadcrumbItem = styled(Breadcrumb.Item)`
  & a {
    color: var(--dark);
  }
`;

type Props = {
  sectionTitle: string;
  currentRoute: string;
  headerSubtitle: string;
};
interface MenuElement {
  title: string;
  routeTo: string;
  icon: string;
}

const Header = ({ sectionTitle, currentRoute, headerSubtitle }: Props) => {
  const [hideHeaderOnResponsive, setHideHeaderOnResponsive] = useState(true);
  const [hideBreadcrumbMenuOnResponsive, setHideBreadcrumbMenuOnResponsive] =
    useState(true);

  return (
    <div className="mb-3">
      <div className="page-header page-header-light">
        <div className="page-header-content header-elements-md-inline">
          <div className="page-title d-flex">
            <h5>
              <i className="mr-2 icon-arrow-left52"></i>
              <span className="font-weight-semibold">
                {sectionTitle}
              </span> - {currentRoute}
              <small className="d-block text-muted">{headerSubtitle}</small>
            </h5>
            <a
              className="header-elements-toggle text-default pointer d-md-none"
              onClick={() => setHideHeaderOnResponsive(!hideHeaderOnResponsive)}
            >
              <i className="icon-more"></i>
            </a>
          </div>
          <div
            className={`header-elements ${hideHeaderOnResponsive && 'd-none'}`}
          >
            <div className="d-flex justify-content-center">
              {MENU_ELEMENTS.map(({ title, icon, routeTo }: MenuElement) => (
                <NavLink
                  key={title}
                  activeClassName="active-link"
                  to={routeTo}
                  className="btn btn-link btn-float text-default"
                >
                  <i className={`${icon} navigationIcons text-primary`}></i>{' '}
                  <span>{title}</span>
                </NavLink>
              ))}
            </div>
          </div>
        </div>

        <div className="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
          <div className="d-flex">
            <Breadcrumb>
              <BreadcrumbItem href="/">
                <i className="mr-2 icon-home2"></i> Home
              </BreadcrumbItem>
              <BreadcrumbItem>{sectionTitle}</BreadcrumbItem>
              <BreadcrumbItem active>{currentRoute}</BreadcrumbItem>
            </Breadcrumb>
            <a
              className="header-elements-toggle text-default pointer d-md-none"
              onClick={() =>
                setHideBreadcrumbMenuOnResponsive(
                  !hideBreadcrumbMenuOnResponsive,
                )
              }
            >
              <i className="icon-more"></i>
            </a>
          </div>

          <div
            className={`header-elements ${
              hideBreadcrumbMenuOnResponsive && 'd-none'
            }`}
          >
            <div className="breadcrumb justify-content-center">
              <a href="#" className="breadcrumb-elements-item">
                <i className="mr-2 icon-comment-discussion"></i>
                Support
              </a>

              <div className="p-0 breadcrumb-elements-item dropdown">
                <a
                  href="#"
                  className="breadcrumb-elements-item dropdown-toggle"
                  data-toggle="dropdown"
                >
                  <i className="mr-2 icon-gear"></i>
                  Settings
                </a>

                <div className="dropdown-menu dropdown-menu-right">
                  <a href="#" className="dropdown-item">
                    <i className="icon-user-lock"></i> Account security
                  </a>
                  <a href="#" className="dropdown-item">
                    <i className="icon-statistics"></i> Analytics
                  </a>
                  <a href="#" className="dropdown-item">
                    <i className="icon-accessibility"></i> Accessibility
                  </a>
                  <div className="dropdown-divider"></div>
                  <a href="#" className="dropdown-item">
                    <i className="icon-gear"></i> All settings
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

Header.propTypes = {
  sectionTitle: PropTypes.string.isRequired,
  currentRoute: PropTypes.string.isRequired,
  headerSubtitle: PropTypes.string.isRequired,
};

export default Header;
