import PropTypes from 'prop-types';
import { Alert, AlertProps } from 'react-bootstrap';

type Props = AlertProps & { body: string };

/**
 * Floating Alert Dismissible
 * @param variant 'info'|'danger'|'success'|'warning"
 */
const FloatingAlert = ({ show, variant, title, body, onClose }: Props) => {
  return (
    <>
      <Alert
        show={show}
        onClose={onClose}
        variant={variant}
        className="bg-white alert-styled-left alert-arrow-left"
        dismissible
      >
        <Alert.Heading as="h6" className="font-weight-semibold">
          {title}
        </Alert.Heading>
        {body}
      </Alert>
    </>
  );
};

FloatingAlert.propTypes = {
  show: PropTypes.bool.isRequired,
  variant: PropTypes.string.isRequired,
  title: PropTypes.string,
  body: PropTypes.string,
  onClose: PropTypes.func,
};

export default FloatingAlert;
