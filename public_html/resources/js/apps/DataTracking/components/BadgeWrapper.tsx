import { Badge } from 'react-bootstrap';
import { STATUS_TYPES } from '../config/data-tracking.config';

type Props = { status: string };

const BadgeWrapper = ({ status }: Props) => (
  <Badge variant={`${STATUS_TYPES[status]?.style || 'info'}`}>
    {STATUS_TYPES[status]?.label}
  </Badge>
);

export default BadgeWrapper;
