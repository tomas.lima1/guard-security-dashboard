import React from 'react';
import { NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';

export interface MenuElement {
  title: string;
  routeTo: string;
  icon: string;
}

type Props = {
  title: string;
  menuElements: MenuElement[];
  children?: React.ReactNode;
};
type NavigationProps = { menuElements: MenuElement[] };

const NavigationCardTab = ({ menuElements }: NavigationProps) => {
  return (
    <>
      <ul className="nav nav-tabs nav-tabs-highlight card-header-tabs">
        {menuElements.map((m) => (
          <li key={m.title} className="nav-item">
            <NavLink
              to={m.routeTo}
              activeClassName="active"
              className="nav-link"
            >
              {m.icon && <i className={`mr-2 ${m.icon}`}></i>}
              {m.title}
            </NavLink>
          </li>
        ))}
      </ul>
    </>
  );
};

NavigationCardTab.propTypes = {
  menuElements: PropTypes.arrayOf(
    PropTypes.shape({
      title: PropTypes.string.isRequired,
      routeTo: PropTypes.string.isRequired,
      icon: PropTypes.string,
    }),
  ).isRequired,
};

const CardWithTabs = ({ title, menuElements, children }: Props) => {
  return (
    <>
      <div className="card">
        <div className="pb-0 card-header bg-light pt-sm-0 header-elements-sm-inline">
          <h6 className="card-title">{title}</h6>
          <div className="header-elements">
            <NavigationCardTab menuElements={menuElements} />
          </div>
        </div>

        <div className={`card-body tab-content ${!children ? 'd-none' : ''}`}>
          {children ? (
            <div className="tab-pane fade show active">{children}</div>
          ) : null}
        </div>
      </div>
    </>
  );
};

CardWithTabs.propTypes = {
  title: PropTypes.string.isRequired,
  menuElements: PropTypes.array.isRequired,
  // children: PropTypes.node,
};

export default CardWithTabs;
