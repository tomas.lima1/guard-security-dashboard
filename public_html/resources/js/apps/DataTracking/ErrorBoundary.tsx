import React, { useState } from 'react'
import { ErrorBoundary as ReactErrorBoundary } from 'react-error-boundary'

type ErrorFallbackType = {
  error: any
  resetErrorBoundary: any
}

type ErrorBoundaryType = { 
  children: React.ReactNode
}

function ErrorFallback({error, resetErrorBoundary}: ErrorFallbackType) {
  return (
    <div className="card" id="errorBoundary" role="alert">
      <div className="card-header header-elements-inline">
        <h2 className="card-title">Something went wrong:</h2>
        <div className="header-elements">
          <div className="list-icons">
            <a className="list-icons-item" data-action="collapse"></a>
            <a className="list-icons-item" data-action="reload"></a>
          </div>
        </div>
      </div>
      
      <div className="card-body">
        <pre>{error.message}</pre>
        <div className="text-center">
          <button className="btn btn-warning mt-2" onClick={resetErrorBoundary}>Try again!</button>
        </div>
      </div>
    </div>
  )
}

export default function ErrorBoundary({ children }: ErrorBoundaryType) {
  const [error, setError] = useState(false)
  return (
    <ReactErrorBoundary
      FallbackComponent={ErrorFallback}
      onReset={() => setError(false)}
      resetKeys={[error]}
    >
      { children }
    </ReactErrorBoundary>
  )
}