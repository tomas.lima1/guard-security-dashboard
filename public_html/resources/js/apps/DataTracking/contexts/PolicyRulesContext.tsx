import { ReactNode, useState } from 'react';
import { createContext } from 'react';
import { PolicyRule } from '../models/policy-rule';

type PolicyRuleState = {
  policyRules: PolicyRule[];
  selectedPolicyRule: PolicyRule | undefined;
  setPolicyRules: (policyRules: PolicyRule[]) => void;
  setSelectedPolicyRule: (PolicyRule: PolicyRule) => void;
};

type Props = {
  children: ReactNode;
};

const initialState: PolicyRuleState = {
  policyRules: [],
  selectedPolicyRule: undefined,
  setPolicyRules: () => null,
  setSelectedPolicyRule: () => null,
};

export const PolicyRulesContext = createContext<PolicyRuleState>(initialState);

export const PolicyRuleProvider = ({ children }: Props) => {
  const [state, setState] = useState<PolicyRuleState>(initialState);

  const setPolicyRules = (policyRules: PolicyRule[]) => {
    setState({
      ...state,
      policyRules,
    });
  };

  const setSelectedPolicyRule = (selectedPolicyRule: PolicyRule) => {
    setState({
      ...state,
      selectedPolicyRule,
    });
  };

  return (
    <PolicyRulesContext.Provider
      value={{
        ...state,
        setPolicyRules,
        setSelectedPolicyRule,
      }}
    >
      {children}
    </PolicyRulesContext.Provider>
  );
};
