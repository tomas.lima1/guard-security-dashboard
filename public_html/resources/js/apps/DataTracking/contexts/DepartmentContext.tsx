import { ReactNode, useState } from 'react';
import { createContext } from 'react';
import { Department } from '../models/department';

type DepartmentState = {
  departments: Department[];
  selectedDepartment: Department | undefined;
  setDepartments: (departments: Department[]) => void;
  setSelectedDepartment: (department: Department) => void;
};

type Props = {
  children: ReactNode;
};

const initialState: DepartmentState = {
  departments: [],
  selectedDepartment: undefined,
  setDepartments: () => null,
  setSelectedDepartment: () => null,
};

export const DepartmentContext = createContext<DepartmentState>(initialState);

export const DepartmentProvider = ({ children }: Props) => {
  const [state, setState] = useState<DepartmentState>(initialState);

  const setDepartments = (departments: Department[]) => {
    setState({
      ...state,
      departments,
    });
  };

  const setSelectedDepartment = (selectedDepartment: Department) => {
    setState({
      ...state,
      selectedDepartment,
    });
  };

  return (
    <DepartmentContext.Provider
      value={{
        ...state,
        setDepartments,
        setSelectedDepartment,
      }}
    >
      {children}
    </DepartmentContext.Provider>
  );
};
