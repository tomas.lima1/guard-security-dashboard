import { ReactNode, useState } from 'react';
import { createContext } from 'react';
import { Request } from '../models/request';

type RequestState = {
  requests: Request[];
  selectedRequest: Request | undefined;
  setRequests: (requests: Request[]) => void;
  setSelectedRequest: (Request: Request) => void;
};

type Props = {
  children: ReactNode;
};

const initialState: RequestState = {
  requests: [],
  selectedRequest: undefined,
  setRequests: () => null,
  setSelectedRequest: () => null,
};

export const RequestContext = createContext<RequestState>(initialState);

export const RequestProvider = ({ children }: Props) => {
  const [state, setState] = useState<RequestState>(initialState);

  const setRequests = (requests: Request[]) => {
    setState({
      ...state,
      requests,
    });
  };

  const setSelectedRequest = (selectedRequest: Request) => {
    setState({
      ...state,
      selectedRequest,
    });
  };

  return (
    <RequestContext.Provider
      value={{
        ...state,
        setRequests,
        setSelectedRequest,
      }}
    >
      {children}
    </RequestContext.Provider>
  );
};
