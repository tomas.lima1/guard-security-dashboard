import { ReactNode, useState } from 'react';
import { createContext } from 'react';
import { Policy } from '../models/policy';

type PolicyState = {
  policies: Policy[];
  selectedPolicy: Policy | undefined;
  setPolicies: (policies: Policy[]) => void;
  setSelectedPolicy: (policy: Policy) => void;
};

type Props = {
  children: ReactNode;
};

const initialState: PolicyState = {
  policies: [],
  selectedPolicy: undefined,
  setPolicies: () => null,
  setSelectedPolicy: () => null,
};

export const PolicyContext = createContext<PolicyState>(initialState);

export const PolicyProvider = ({ children }: Props) => {
  const [state, setState] = useState<PolicyState>(initialState);

  const setPolicies = (policies: Policy[]) => {
    setState({
      ...state,
      policies,
    });
  };

  const setSelectedPolicy = (selectedPolicy: Policy) => {
    setState({
      ...state,
      selectedPolicy,
    });
  };

  return (
    <PolicyContext.Provider
      value={{
        ...state,
        setPolicies,
        setSelectedPolicy,
      }}
    >
      {children}
    </PolicyContext.Provider>
  );
};
