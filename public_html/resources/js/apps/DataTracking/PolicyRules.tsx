import { format } from 'date-fns';
import { useEffect, useMemo } from 'react';
import { Button, Col, Container, Form, Row } from 'react-bootstrap';
import { useFieldArray, useForm, useFormState } from 'react-hook-form';
import { useQuery } from 'react-query';
import { useHistory } from 'react-router';
import { useParams } from 'react-router-dom';
import styled from 'styled-components';
import getPolicy from './api/getPolicy';
import CardBasic from './components/CardBasic';
import FormControlFeedback from './components/FormControlFeedback';
import Header from './components/Header';
import truncateString from './helpers/truncateString';
import useToastAlerts from './hooks/useToastAlerts';
import { Policy } from './models/policy';

type FormValues = Policy;

const RuleContainer = styled(Row)`
  display: flex;
  align-items: baseline;
`;

const RuleDelete = styled(Col)`
  cursor: pointer;
`;

const RuleHeaderWrapper = styled.div`
  display: flex;
  justify-content: space-around;

  @media screen and (max-width: 720px) {
    display: block;
    text-align: left;
  }
`;

const PolicyRules = () => {
  const { addToast } = useToastAlerts();
  const history = useHistory();
  const { id } = useParams<{ id: string }>();
  const { data, isLoading } = useQuery(['policy', id], () => getPolicy(id), {
    keepPreviousData: true,
    onError: ({ error }) =>
      addToast(`An erorr has occurred, please try again in few seconds!`, {
        appearance: 'error',
        autoDismiss: true,
      }),
  });
  const policy = useMemo(() => data?.data, [data]);

  return (
    <>
      <Header
        sectionTitle="User Data Traceability"
        headerSubtitle="Policies control"
        currentRoute="Edit policy rules"
      />
      <CardBasic title={`Edit policy's rules of policy: ${policy?.name}`}>
        <Container>
          <Row className="justify-content-center">
            <Col lg="10">
              {policy && (
                <Form>
                  <Col className="mb-3">
                    <fieldset>
                      <legend>Description:</legend>
                      {policy.description}
                    </fieldset>
                  </Col>
                  <Col>
                    <div
                      className="card-group-control card-group-control-right"
                      id={`rules-accordion`}
                    >
                      {policy && <PolicyRulesFormArray policy={policy} />}
                    </div>
                  </Col>
                  <Col sm lg={8} className="mx-auto">
                    <Form.Group as={Row} controlId="enable">
                      <Col>
                        <Button
                          title="Go back to the preview page visited"
                          block
                          variant="outline-secondary"
                          type="button"
                          onClick={() => history.goBack()}
                        >
                          Back
                        </Button>
                      </Col>
                      <Col>
                        <Button title="Edit policy" block variant="info" type="submit">
                          Edit policy
                        </Button>
                      </Col>
                      <Col>
                        <Button title="Add new policy rule" block variant="primary" type="button">
                          Add policy rule
                        </Button>
                      </Col>
                    </Form.Group>
                  </Col>
                </Form>
              )}
            </Col>
          </Row>
        </Container>
      </CardBasic>
    </>
  );
};

const PolicyRulesFormArray = ({ policy }: { policy: Policy }) => {
  const methods = useForm<FormValues>({
    defaultValues: {
      policyRules: policy.policyRules,
    },
  });
  const { register, control, setValue } = methods;
  const { errors } = useFormState({ control });
  const { fields } = useFieldArray({
    control,
    name: 'policyRules',
    shouldUnregister: true,
  });

  useEffect(() => {
    setValue('policyRules', policy.policyRules);
  }, [policy]);

  return (
    <>
      {fields.map((item, index) => (
        <RuleContainer key={item.id}>
          <Col>
            <div className="card">
              <div className="card-header bg-secondary">
                <h6 className="card-title">
                  <a
                    data-toggle="collapse"
                    className="collapsed text-white w-100"
                    href={`#accordion-${index}`}
                  >
                    <RuleHeaderWrapper>
                      <div title={item.name}>{truncateString(item.name, 12)}</div>
                      <div>{format(new Date(item.createdAt), 'Pp')}</div>
                      <div>
                        Priority <b>{item.priority}</b>{' '}
                      </div>
                    </RuleHeaderWrapper>
                  </a>
                </h6>
              </div>

              <div id={`accordion-${index}`} className="collapse" data-parent="#rules-accordion">
                <div className="card-body">
                  <Form.Group as={Row} controlId={`policyRules.${index}.name`}>
                    <Form.Label column sm="3">
                      Name
                    </Form.Label>
                    <Col sm="9">
                      <Form.Control
                        type="text"
                        placeholder="(*) A name for your policy"
                        {...register(`policyRules.${index}.name` as const)}
                        isInvalid={!!errors?.policyRules?.[index]?.name}
                      />
                      <FormControlFeedback errors={errors} name="name" />
                    </Col>
                  </Form.Group>
                  <Form.Group as={Row} controlId={`policyRules.${index}.description`}>
                    <Form.Label column sm="3">
                      Description
                    </Form.Label>
                    <Col sm="9">
                      <Form.Control
                        as="textarea"
                        aria-describedby={`policyRules.${index}.description`}
                        rows={2}
                        placeholder="Describe the purpose of this policy"
                        {...register(`policyRules.${index}.description` as const)}
                      />
                    </Col>
                  </Form.Group>
                  <Form.Group as={Row} controlId={`policyRules.${index}.priority`}>
                    <Form.Label column sm="3">
                      Priority
                    </Form.Label>
                    <Col sm="9">
                      <Form.Control
                        as="input"
                        type="number"
                        aria-describedby={`policyRules.${index}.priority`}
                        {...register(`policyRules.${index}.priority` as const)}
                        isInvalid={!!errors?.policyRules?.[index]?.priority}
                      />
                      <FormControlFeedback errors={errors} name="priority" />
                    </Col>
                  </Form.Group>
                </div>
              </div>
            </div>
          </Col>
          <RuleDelete md="1">
            <Button variant="link" className="text-danger">
              delete
            </Button>
          </RuleDelete>
        </RuleContainer>
      ))}
    </>
  );
};

export default PolicyRules;
