import { MenuElement } from '../components/CardWithTabs';

export const API_URL =
  process.env.MIX_DATA_TRACKING_API_URL || 'http://localhost:4002';
export const URL_REGEX =
  /^(?:([a-z0-9+.-]+):\/\/)(?:\S+(?::\S*)?@)?(?:(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*\.?)(?::\d{2,5})?(?:[/?#]\S*)?$/i;

export const MENU_ELEMENTS: Array<MenuElement> = [
  {
    title: 'Requests',
    routeTo: '/requests',
    icon: 'mi-room-service',
  },
  {
    title: 'Policies',
    routeTo: '/policies',
    icon: 'mi-security',
  },
  {
    title: 'Departments',
    routeTo: '/departments',
    icon: 'mi-people',
  },
];

export const STATUS_TYPES: {
  [key: string]: {
    label: 'PENDING' | 'APPROVED' | 'DENIED' | 'CANCELED';
    style: string;
  };
} = {
  PENDING: {
    label: 'PENDING',
    style: 'warning',
  },
  APPROVED: {
    label: 'APPROVED',
    style: 'success',
  },
  DENIED: {
    label: 'DENIED',
    style: 'danger',
  },
  CANCELED: {
    label: 'CANCELED',
    style: 'dark',
  },
};

export const DATATABLE_SORTING_CLASSNAME = {
  isAsc: 'sorting_asc',
  isDesc: 'sorting_desc',
  default: 'sorting',
};
