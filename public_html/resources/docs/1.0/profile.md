# Profile

---

- [User Profile](#profile)
- [Account Settings](#account)
- [Users List](#users)

<a name="section-1"></a>
## User Profile

The user profile pages are accessible form the user dropdown located on the right side of the main navigation bar, on the top of the page.
The user profile feature allows the user to update profile related information. Such as name, email.

<br>

![image info](../../../images/docs/profile.png)

<a name="section-2"></a>
## Account Setting

The account settings enable the user to update the password and the role, if the user has enough permissions to change the role.

<a name="section-3"></a>
## Users List

If logged in as and administrator, the user will also be able to see the **Users List** page. In this page, 
the user can all existing users, and their access status (approved, pending, blocked), and roles. The admin user will be 
able to change the the status, role and delete the user.