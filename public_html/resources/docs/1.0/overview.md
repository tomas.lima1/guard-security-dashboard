# Overview


This documentation describes the functionalities and features of the GUARD Security Dashboard.

To access the Dashboard use this link <a href="https://dashboard.guard-project.eu">dashboard.guard-project.eu</a>.


---

- [Welcome page](#section-welcome)
- [Dashboard Design & Structure](#section-structure)

<a name="section-welcome"></a>
## Welcome Page

The Welcome Page is the landing page of the application after successful user logged in.
On the welcome page the user is presented with the main functionalities of the dashboard and can easily navigate to the
desired page by using the buttons in the welcome page or the side bar located in the left side. 

<a name="section-structure"></a>
## Dashboard Design & Structure

![image info](../../../images/docs/welcome.png)

In any section or page of the dashboard the main navigation bar and the side bar are always available for the user.
The main bar includes a dropdown bar on the far right side that allows the user to access its profile, account settings and
perform a logout and a reduce button on the left that controls the size of the side bar.

The sidebar displays information of the current user and a main menu which presents the different pages of the dashboard
available for the specific user.

Under the main bar is placed the page title section. And below the title section the user will find the breadcrumbs section 
that shows the current navigation of the user in the dashboard and allows the user to return to hierarchical higher pages.

After the breadcrumbs section is where the specific page content is placed. Finally, at the bottom of the page the footer is presented.

The dashboard uses the colors of the GUARD project which includes a GUARD blue, dark blue, and grey. 
And it uses specific researched icons and styles for the GUARD project. 

