# Service Topology

---

- [Description](#description)
- [Service Details](#details)

<a name="description"></a>
## Description

The service topology visualization feature displays to the user the overall topology of services in the GUARD framework 
in an interactive network visualization. It simplifies the complex relations between the different services and 
environments in the GUARD system. This can be seen in the image below.

![image info](../../../images/docs/service_topology.png)

<a name="details"></a>
## Service Details

The service details feature provides detailed information about the selected service or service connection. 
This information can include hostname, stage, status, description, service type, responsible partner (organisation), 
LCP and connection information. An example of the information can be seen on the right side of the image in the chapter above.