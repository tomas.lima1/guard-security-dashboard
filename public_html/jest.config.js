module.exports = {
  moduleFileExtensions: ["ts", "tsx", "js", "jsx", "json", "node"],
  'transform': {
    "^.+\\.tsx?$": "ts-jest",
    '^.+\\.js$': '<rootDir>/node_modules/babel-jest'
  },
  "setupFilesAfterEnv": ['<rootDir>/resources/js/setupTests.tsx']

}