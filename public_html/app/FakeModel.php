<?php


namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * NOTE: this shall be used to mimic a model collection where one can set/getAttributes
 * Class FakeModel
 * @package App\Classes

 */
class FakeModel extends Model
{

}
