<?php

namespace App\Http\Controllers;

use App\Http\API\ContextBrokerApiTrait;
use Elasticsearch\ClientBuilder;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class securityDashboardController extends Controller
{

    use ContextBrokerApiTrait;

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $data = $this->getData();

        return view('dashboard', $data);
    }

    public function getData()
    {
        $response['pipelines'] = count($this->getPipelines());
        $response['notifications'] = $this->countNotifications();
        $response['services'] = $this->countExecuteEnvironments();

        return $response;
    }

    /**
     * This function establish connection to ES Client
     *
     * @return \Elasticsearch\Client
     */
    public function esConnection() {
        $esHost = config('elasticquent.config.hosts');
        return ClientBuilder::create()  // Instantiate a new ClientBuilder
        ->setHosts($esHost)             // Set the hosts
        ->build();
    }

    /**
     * This function loads the data from ElasticSearch based on the search params.
     *
     * @param $params
     * @return number
     */
    public function countNotifications() {
        $es = $this->esConnection();

        try {
            $results = $es->search([
                'index' => 'notification-index',
                'size' => 1000,
            ]);
            $hits = $this->restrictData($results);

            return $hits;
        }
        catch(\Exception $ex) {
            \Log::critical($ex);
            return 0;
        }
    }


    /**
     * @param $resultData
     * @return number
     * @uses restricts only data relevant to threat notifications.
     */
    public function restrictData($resultData){
        $count = 0;
        if (!empty($resultData['hits']) && !empty($resultData['hits']['hits'])) {
            foreach ($resultData['hits']['hits'] as $key => $data) {
                if(!empty($data['_source']) && !empty($data['_source']['@timestamp']) && !empty($data['_source']['TIMESTAMP'])){
                    $count++;
                }
            }
        }

        return $count;
    }
}
