<?php


namespace App\Http\Controllers;

use App\Classes\ServiceTopologyHelper;
use App\Http\API\ContextBrokerApiTrait;

class serviceTopologyController extends Controller
{

    use ContextBrokerApiTrait;

    public function index()
    {
        $rawEnv = $this->getExecuteEnvironmentsTopology();
        $environments = $this->addServicePositionEnvironment($rawEnv);
        $connections = $this->getConnection();
        $networks = $this->getNetworkLink();
        $connectionList = $this->prepareConnectionList($connections, $networks, $rawEnv);

        //check if needed CB data is available to create topology.
        if(count($rawEnv) && count($connections) && count($networks)){
            $data = [
                'environments' => $environments,
                'networks' => $networks,
                'connections' => $connectionList,
                'error' => false
            ];
            return view('service-topology.index', $data);
        }else{
            $data = [
                'environments' => [],
                'networks' => [],
                'connections' => [],
                'error' => true
            ];
            return view('service-topology.index', $data);
        }
    }

    public function addServicePositionEnvironment($environments)
    {
        for($i = 0; $i<count($environments); $i++){
            $environments[$i]['position'] = ServiceTopologyHelper::addPosition($environments[$i]['id']);
        }
        return $environments;
    }

    public function prepareConnectionList($connections, $networks, $envs)
    {
        $list = [];
        $index = 0;
        foreach ($connections as $connection){

            //check if connection envs exist in env list. If does not exist should not be part of the connections.
            if(!$this->checkConEnvs($connection, $envs)){
                continue;
            }

            $list[$index]['node1'] = $connection['exec_env_id'];

            if(strpos($connection['id'],'2')){
                $nodes = explode('2', $connection['id']);
                $list[$index]['node2'] = $nodes[1];
                $list[$index]['connection_type'] = $this->getConnectionType($connection['network_link_id'], $networks);
            }else if(strpos($connection['id'], '>')){
                $nodes = explode('>', $connection['id']);
                if(strpos($nodes[1], '@')){
                    $temp = explode('@', $nodes[1]);
                    $list[$index]['node2'] = $temp[1];
                    $list[$index]['connection_type'] = $temp[0];
                }else{
                    $networkType = $this->getConnectionType($connection['network_link_id'], $networks);
                    $networkLinks = $this->getNetworkLinkInConnection($connection['network_link_id'], $connections);
                    if(!empty($networkLinks) and count($networkLinks) > 1){
                        $list[$index]['node1'] = $networkLinks[0]['exec_env_id'];
                        $list[$index]['node2'] = $networkLinks[1]['exec_env_id'];
                        $list[$index]['connection_type'] = $networkType;
                    }
                    $list = array_unique($list, SORT_REGULAR);//TODO-Test.
                }
            }
            $index++;
        }

        $list = array_values($list);
        return $list;
    }

    public function getConnectionType($networkLink, $networks)
    {
        $result = "";
        foreach ($networks as $network){
            if($network['id'] === $networkLink){
                $result = $network['type_id'];
            }
        }
        return $result;
    }

    public function getNetworkLinkInConnection($networkLinkId, $connections){
        $foundNetworks = [];
        foreach($connections as $connection){
            if($connection['network_link_id'] ===  $networkLinkId){
                $foundNetworks[] = $connection;
            }
        }

        return $foundNetworks;
    }

    public function checkNetworAndEnvs($con, $networks, $envs)
    {
        $networkExists = false;
        $envExists = false;
        foreach ($networks as $net){
            if($con['network_link_id'] === $net['id']){
                $networkExists = true;
            }
        }

        foreach ($envs as $env){
            if($con['exec_env_id'] === $env['id']){
                $envExists = true;
            }
        }

        if(isset($networkExists) && isset($envExists)){
            return true;
        }

        return false;
    }

    public function checkConEnvs($con, $envs)
    {
        foreach ($envs as $env){
            if($con['exec_env_id'] === $env['id']){
                return true;
            }
        }
        return false;
    }
}