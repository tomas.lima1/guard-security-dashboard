<?php


namespace App\Http\Controllers;
use Elasticsearch\ClientBuilder;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

class notificationsController extends Controller
{
    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        return view('kafka-notifications.index');
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajaxNotificationsTable()
    {
        $params = [
            'index' => 'notification-index',
            'size' => 1000,
        ];

        $notifications = $this->prepareData($params);

        return datatables($notifications)->toJson();
    }

    /**
     * @param $params
     * @return array
     */
    public function prepareData($params){
        $notifications = [];
        $resultData = $this->loadDataFromEs($params);
        if (!empty($resultData) && !empty($resultData['hits'])) {
            foreach ($resultData['hits'] as $key => $data) {
                if(!empty($data['_source']) && !empty($data['_source']['@timestamp'])){
                    //limiting results. Only displaying the ones with the right structure.
                    if(!empty($data['_source']['TIMESTAMP'])){
                        $notifications[$key] = $data['_source'];
                        $notifications[$key]['esTimestamp'] = $data['_source']['@timestamp'];
                    }
                }
            }
        }

        return $notifications;
    }

    /**
     * Find  latest notification from elasticsearch by timestamp
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajaxReloadNotificationsTable()
    {
        $notifications = [];
        if(!empty($_GET['esTimestamp'])) {
            $params = [
                'index' => 'notification-index',
                'size' => 1000,
                'body' => [
                    'query' => [
                        "range" => [
                            "@timestamp" => [
                                "gt" => $_GET['esTimestamp'],
                            ],
                        ],
                    ],
                ],
            ];

            $notifications = $this->prepareData($params);
        }

        return datatables($notifications)->toJson();
    }

    public function ajaxCheckNewNotifications(){
        $newTimestamp = null;
        $status = false;
        if(!empty($_GET['esTimestamp'])) {
            $params['body'] = [
                "_source" => ['@timestamp'],
                "size"=> 1,
                "sort"=> [
                    "@timestamp"=> [
                    "order"=> "desc"
                  ],
                ],
            ];

            $resultData = $this->loadDataFromEs($params);
            if(isset($resultData['hits'][0]['_source']['@timestamp']) &&
                $_GET['esTimestamp'] !== $resultData['hits'][0]['_source']['@timestamp']){
                $status = true;
                $newTimestamp = $resultData['hits'][0]['_source']['@timestamp'];
            }
        }

        return response()->json(['newTimestamp' => $newTimestamp, 'status' => $status]);
    }

    /**
     * This function establish connection to ES Client
     *
     * @return \Elasticsearch\Client
     */
    public function esConnection() {
        $esHost = config('elasticquent.config.hosts');
        return ClientBuilder::create()  // Instantiate a new ClientBuilder
        ->setHosts($esHost)             // Set the hosts
        ->build();
    }

    /**
     * This function loads the data from ElasticSearch based on the search params.
     *
     * @param $params
     * @return mixed
     */
    public function loadDataFromEs($params) {
        $es = $this->esConnection();

        try {
            $results = $es->search($params);
            $hits = $results['hits'];

            return $hits;
        }
        catch(\Exception $ex) {
            \Log::critical($ex);
            return [];
        }
    }

    /**
     * This function is used to test notification with faked entries
     *
     * @return array[]
     */
    public function feedFakeData()
    {
        return [
            [
                "TIMESTAMP" => time(),
                "SEVERITY" => "10",
                "DESCRIPTION" => "DDoS Attack(s)",
                "DATA" => [
                    "212.31.108.19" => [
                        "GoldenEye" => 1
                    ]
                ],
                "SOURCE" => "pgafilter2",
                "@version" => "1",
                "@timestamp" => time(),
                "esTimestamp" => time(),
            ]
        ];
    }

}